/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.tools.unlabeled

import scala.util.control.Breaks._

object TextToTokenized extends App {

  breakable {
    while (true) {

      println("while")
      break
    }

  }
  println("break reached here")

}