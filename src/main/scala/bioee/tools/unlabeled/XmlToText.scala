/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.tools.unlabeled

import edu.uwm.pmcarticleparser.PMCArticle
import java.io.File
import bioee.utils.TextHelpers
import java.io.PrintWriter
import bioee.text.segmentation.tokenization.WhitespaceTokenizer
import bioee.text.segmentation.tokenization.SimpleTokenizer
import scala.collection.JavaConversions._
import com.google.common.base.CharMatcher
import edu.arizona.sista.processors.corenlp.CoreNLPProcessor
import bioee.utils.BioNLPUtils
import bioee.utils.Word2Index
import bioee.utils.Word2Vec
import bioee.utils.BrownCluster

object XmlToText extends App {
  import bioee.utils.RichFile
  import bioee.utils.RichFile.enrichFile

  brownPrefix("/home/tsendee/pubmed/data/BioNLP-ST_2011_genia_train_data_rev1/", "/home/tsendee/pubmed/data/train.brown.6", "/home/tsendee/pubmed/data/merged-abs-simple-tokenized-c1000-p1.txt")
  suffixLetters("/home/tsendee/pubmed/data/BioNLP-ST_2011_genia_train_data_rev1/", "/home/tsendee/pubmed/data/train.suffix.3")
  //  Index2Vec
  //  wordFreq("/home/tsendee/pubmed/data/BioNLP-ST_2011_genia_train_data_rev1/", "/home/tsendee/pubmed/data/train.wf")
  //  coreNLPTokenize()
  //  toLetterNgram("/home/tsendee/pubmed/corpus/simple-tokenized.corpus.lcase.non-numeric")

  def mergeGeniaText() = {
    CharMatcher.DIGIT.retainFrom("")
  }

  def joinChemdnerAbs() = {
    val allAbsPath = "/home/tsendee/pubmed/chemdner_unlabeled/all-abstracts.txt"
    val testAbsPath = "/home/tsendee/pubmed/chemdner_unlabeled/chemdner_abs_test.txt"
    val devAbsPath = "/home/tsendee/pubmed/chemdner_unlabeled/chemdner_abs_development.txt"
    val trainAbsPath = "/home/tsendee/pubmed/chemdner_unlabeled/chemdner_abs_training.txt"
    val mergedOut = new PrintWriter(new File("/home/tsendee/pubmed/chemdner_unlabeled/merged-abs.txt"))
    def printlnLine(inputFile: File) = {
      inputFile.lines.foreach { line =>
        mergedOut.println(line.split("\t").takeRight(2).mkString(" "))
      }
    }
    try {
      List(allAbsPath, testAbsPath, devAbsPath, trainAbsPath).foreach { filePath =>
        printlnLine(new File(filePath))
      }
    } finally {
      mergedOut.close
    }
  }

  def tokenizeCorpus = {
    val corpusPath = "/home/tsendee/pubmed/corpus/genia-merged"
    val corpus = new File(corpusPath)
    val whitespaceTokenizer = new WhitespaceTokenizer
    val simpleTokenizer = new SimpleTokenizer
    val whitespaceTokenizedOut = new PrintWriter(new File("/home/tsendee/pubmed/corpus/genia-merged-whitespace-tokenized.corpus"), "UTF-8")
    val simpleTokenizedOut = new PrintWriter(new File("/home/tsendee/pubmed/corpus/genia-merged-simple-tokenized.corpus"), "UTF-8")
    try {
      corpus.lines.foreach { line =>
        whitespaceTokenizedOut.println(whitespaceTokenizer.getTokens(line).toList.mkString(" "))
        simpleTokenizedOut.println(simpleTokenizer.getTokens(line).toList.mkString(" "))
      }
    } finally {
      whitespaceTokenizedOut.close
      simpleTokenizedOut.close
    }
  }

  def coreNLPTokenize() = {
    val coreNLPProcessor = new CoreNLPProcessor()
    val corpusPath = "/home/tsendee/pubmed/corpus/whitespace-tokenized.corpus"
    val corpus = new File(corpusPath)
    val tokenizedOut = new PrintWriter(new File("/home/tsendee/pubmed/corpus/core-nlp-tokenized.corpus"), "UTF-8")
    try {
      corpus.lines.foreach { line =>
        val tLine = coreNLPProcessor.mkDocument(line).sentences.map { sent =>
          sent.words.mkString(" ")
        }.mkString(" ")
        tokenizedOut.println(tLine)
      }
    } finally {
      tokenizedOut.close()
    }
  }

  def toLCase(path: String) = {
    val corpusOut = new PrintWriter(new File(path + ".lcase"), "UTF-8")
    val corpusIn = new File(path)
    try {
      corpusIn.lines.foreach(line => corpusOut.println(line.toLowerCase))
    } finally {
      corpusOut.close()
    }

  }

  def replaceNumeric(path: String) = {
    val corpusOut = new PrintWriter(new File(path + ".non-numeric"), "UTF-8")
    val corpusIn = new File(path)
    try {
      corpusIn.lines.foreach { line =>
        val pLine = line.split(" ").map { token =>
          TextHelpers.normNumeric(token)
        }.mkString(" ")
        corpusOut.println(pLine)
      }
    } finally {
      corpusOut.close()
    }
  }

  def toLetterNgram(path: String, ngram: Int = 3) = {
    val corpusOut = new PrintWriter(new File(path + "." + ngram + "-gram"), "UTF-8")
    val corpusIn = new File(path)
    try {
      corpusIn.lines.foreach { line =>
        val lineLetterNgram = line.split(" ").flatMap { token =>
          TextHelpers.letterNgram(token, ngram)
        }.mkString(" ")
        corpusOut.println(lineLetterNgram)
      }
    } finally {
      corpusOut.close()
    }
  }

  def xmlToText = {
    val corpusOut = new PrintWriter(new File("/home/tsendee/pubmed/pmc/pmc.corpus"), "UTF-8")
    val pmcDirPath = "/home/tsendee/pubmed/pmc"
    val pmcDir = new File(pmcDirPath)
    val nxmlFiles = RichFile.childFiles(pmcDir, ".nxml")
    val tsvOut = new PrintWriter(new File("/home/tsendee/pubmed/pmc/pmc.tsv"), "UTF-8")
    try {
      nxmlFiles.foreach { nxml =>
        val pmcArticle = new PMCArticle(nxml.getAbsolutePath)
        val pmcId = pmcArticle.getPmcId
        val title = TextHelpers.replaceSpaces(pmcArticle.getTitle)
        val abs = TextHelpers.replaceSpaces(pmcArticle.getAbstractText)
        val fullText = TextHelpers.replaceSpaces(pmcArticle.getFullTextText())
        tsvOut.println(s"$pmcId\t$title\t$abs\t$fullText")
        corpusOut.println(s"$title $abs $fullText")
      }
    } finally {
      tsvOut.close
      corpusOut.close
    }
  }

  def wordFreq(dir: String, out: String) = {
    val o = new File(out)
    var w2f = Map[String, Int](TextHelpers.UNKNOWN -> 1000)
    val simpleTokenizer = new SimpleTokenizer
    val docs = BioNLPUtils.getFiles(new File(dir), ".txt")
    docs.foreach { doc =>
      val txt = doc.lines.mkString("\n")
      simpleTokenizer.getTokens(txt).toList.filter(TextHelpers.replaceSpaces(_).length() > 0).foreach { t =>
        val tn = t.trim() //TextHelpers.normNumeric(t.toLowerCase())
        w2f = w2f + (tn -> (w2f.getOrElse(tn, 0) + 1))
      }
    }
    println(w2f.filter(_._2 > 5).size)
    o.lines = w2f.toArray.sortBy(_._2).map(p => p._1 + "\t" + p._2).toIterator
  }

  def suffixLetters(dir: String, out: String) = {
    val o = new File(out)
    var w2f = Map[String, Int](TextHelpers.UNKNOWN -> 1000)
    val simpleTokenizer = new SimpleTokenizer
    val docs = BioNLPUtils.getFiles(new File(dir), ".txt")
    docs.foreach { doc =>
      val txt = doc.lines.mkString("\n")
      simpleTokenizer.getTokens(txt).toList.filter(TextHelpers.replaceSpaces(_).length() > 0).foreach { t =>
        val tn = TextHelpers.nSuffix(t.trim())
        w2f = w2f + (tn -> (w2f.getOrElse(tn, 0) + 1))
      }
    }
    println(w2f.filter(_._2 > 5).size)
    o.lines = w2f.toArray.sortBy(_._2).map(p => p._1 + "\t" + p._2).toIterator
  }

  def brownPrefix(dir: String, out: String, brownModelPath: String) = {
    val bCluster = new BrownCluster(pathsToClusterFile = brownModelPath)
    val o = new File(out)
    var w2f = Map[String, Int](TextHelpers.UNKNOWN -> 1000)
    val simpleTokenizer = new SimpleTokenizer
    val docs = BioNLPUtils.getFiles(new File(dir), ".txt")
    docs.foreach { doc =>
      val txt = doc.lines.mkString("\n")
      simpleTokenizer.getTokens(txt).toList.filter(TextHelpers.replaceSpaces(_).length() > 0).foreach { t =>
        val preffix = bCluster.wordPrefix(t.trim(), 20) //992 > 20
        if (preffix.length() > 0) {
          w2f = w2f + (preffix -> (w2f.getOrElse(preffix, 0) + 1))
        }
      }
    }
    println(w2f.filter(_._2 > 5).size)
    o.lines = w2f.toArray.sortBy(_._2).map(p => p._1 + "\t" + p._2).toIterator
  }

  def Index2Vec() = {
    val w2i = new Word2Index
    w2i.loadVoc("/home/tsendee/pubmed/data/train.wf", 5)
    val w2v = new Word2Vec
    w2v.load("/home/tsendee/pubmed/corpus/simple-tokenized-sz200.wv")
    val f = new File("/home/tsendee/pubmed/data/train-sz200.i2v")
    val i2vs = w2i.t2i.foldLeft(List[String]()) { (s, p) =>
      val vec = w2v.vector(p._1)
      if (vec.length > 0) {
        s :+ (p._2 + "," + vec.mkString(","))
      } else {
        s
      }
    }
    f.lines = i2vs.toIterator
  }
}