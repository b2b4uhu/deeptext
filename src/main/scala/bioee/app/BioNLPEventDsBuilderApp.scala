/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.app

import bioee.components.BioNLPSimpleTokenizerBasedDatasetBuilder
import bioee.utils.Conf
import bioee.dataset.generation.BioNLPExampleBuilderComponent
import bioee.text.preprocessing.BioNLPPreprocessingComponent
import bioee.components.BioNLPSimpleTokenizerBasedDatasetBuilder
import bioee.utils.BioNLPConstants
import bioee.domains.Dataset
import com.typesafe.scalalogging.slf4j.LazyLogging
import bioee.components.BioNLPDsBuilderWithTokenFilter
import bioee.components.BioNLPEventDsBuilder

object BioNLPEventDsBuilderApp extends App with BioNLPEventDsBuilder with LazyLogging {

  val trainDir = conf.root.getString("genia.corpus.train.dir")
  val devDir = conf.root.getString("genia.corpus.dev.dir")
  val testDir = conf.root.getString("genia.corpus.test.dir")

  val simpleProtTriThemeDirTrain = conf.root.getString("simple.prot-tri.theme.dir.train")
  val simpleProtTriProtDirTrain = conf.root.getString("simple.prot-tri-prot.dir.train")
  val complexEventTriThemeDirTrain = conf.root.getString("complex.event-tri.theme.dir.train")
  val complexEventTriProtCauseDirTrain = conf.root.getString("complex.event-tri-prot.cause.dir.train")
  val sentTrain = conf.root.getString("sent.train")

  val simpleProtTriThemeDirDev = conf.root.getString("simple.prot-tri.theme.dir.dev")
  val simpleProtTriProtDirDev = conf.root.getString("simple.prot-tri-prot.dir.dev")
  val complexEventTriThemeDirDev = conf.root.getString("complex.event-tri.theme.dir.dev")
  val complexEventTriProtCauseDirDev = conf.root.getString("complex.event-tri-prot.cause.dir.dev")
  val sentDev = conf.root.getString("sent.dev")

  val simpleProtTriThemeDatasetTrain: Dataset = Dataset(datasetId = BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI,
    description = "BioNLP simple events", examples = Seq(), datasetDirPath = simpleProtTriThemeDirTrain,
    negativeLabel = BioNLPConstants.ThemeNone, addNewLabel = true)
  val simpleProtTriProtTrain = Dataset(datasetId = BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI_PROT,
    description = "BioNLP binding pairs", examples = Seq(), datasetDirPath = simpleProtTriProtDirTrain,
    negativeLabel = BioNLPConstants.None, addNewLabel = true)
  val complexEventTriThemeDatasetTrain = Dataset(datasetId = BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI,
    description = "BioNLP complex events", examples = Seq(), datasetDirPath = complexEventTriThemeDirTrain,
    negativeLabel = BioNLPConstants.ThemeNone, addNewLabel = true)
  val complexEventTriProtCauseDatasetTrain = Dataset(datasetId = BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_PROT,
    description = "BioNLP complex events", examples = Seq(), datasetDirPath = complexEventTriProtCauseDirTrain,
    negativeLabel = BioNLPConstants.CauseNone, addNewLabel = true)

  logger.info("building train dataset...")

  val datasetsTrain = datasetBuilder.buildFromDir(dataSourceDir = trainDir, sentDir = sentTrain,
    simpleProtTriDs = simpleProtTriThemeDatasetTrain,
    simpleProtTriProtDs = simpleProtTriProtTrain,
    complexEventTriThemeDs = complexEventTriThemeDatasetTrain,
    complexEventTriProtCauseDs = complexEventTriProtCauseDatasetTrain)

  datasetsTrain.foreach(kv => kv._2.save(kv._2.datasetDirPath + ".dataset"))

  val simpleProtTriThemeDatasetDev: Dataset = Dataset(datasetId = BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI,
    description = "BioNLP simple events", examples = Seq(), datasetDirPath = simpleProtTriThemeDirDev,
    negativeLabel = BioNLPConstants.ThemeNone, addNewLabel = false, labelIndex = simpleProtTriThemeDatasetTrain.labelIndex)
  val simpleProtTriProtDev = Dataset(datasetId = BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI_PROT,
    description = "BioNLP binding pairs", examples = Seq(), datasetDirPath = simpleProtTriProtDirDev,
    negativeLabel = BioNLPConstants.None, addNewLabel = false, labelIndex = simpleProtTriProtTrain.labelIndex)
  val complexEventTriThemeDatasetDev = Dataset(datasetId = BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI,
    description = "BioNLP complex events", examples = Seq(), datasetDirPath = complexEventTriThemeDirDev,
    negativeLabel = BioNLPConstants.ThemeNone, addNewLabel = false, labelIndex = complexEventTriThemeDatasetTrain.labelIndex)
  val complexEventTriProtCauseDatasetDev = Dataset(datasetId = BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_PROT,
    description = "BioNLP complex events", examples = Seq(), datasetDirPath = complexEventTriProtCauseDirDev,
    negativeLabel = BioNLPConstants.CauseNone, addNewLabel = false, labelIndex = complexEventTriProtCauseDatasetTrain.labelIndex)

  logger.info("building dev dataset...")

  val datasetsDev = datasetBuilder.buildFromDir(dataSourceDir = devDir, sentDir = sentDev,
    simpleProtTriDs = simpleProtTriThemeDatasetDev,
    simpleProtTriProtDs = simpleProtTriProtDev,
    complexEventTriThemeDs = complexEventTriThemeDatasetDev,
    complexEventTriProtCauseDs = complexEventTriProtCauseDatasetDev)

  datasetsDev.foreach(kv => kv._2.save(kv._2.datasetDirPath + ".dataset"))

}