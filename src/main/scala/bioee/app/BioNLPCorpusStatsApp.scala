/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.app

import bioee.components.BioNLPSimpleTokenizerBasedDatasetBuilder
import bioee.utils.Conf
import bioee.dataset.generation.BioNLPExampleBuilderComponent
import bioee.text.preprocessing.BioNLPPreprocessingComponent
import bioee.components.BioNLPSimpleTokenizerBasedDatasetBuilder
import bioee.utils.BioNLPConstants
import bioee.domains.Dataset
import com.typesafe.scalalogging.slf4j.LazyLogging
import bioee.components.BioNLPDsBuilderWithTokenFilter

object BioNLPCorpusStatsApp extends App with BioNLPDsBuilderWithTokenFilter with LazyLogging {

  val trainDir = conf.root.getString("genia.corpus.train.dir")
  val devDir = conf.root.getString("genia.corpus.dev.dir")
  val testDir = conf.root.getString("genia.corpus.test.dir")

  val simpleProtTriThemeDirTrain = conf.root.getString("simple.prot-tri.theme.dir.train")
  val simpleProtTriProtCauseDirTrain = conf.root.getString("simple.prot-tri-prot.cause.dir.train")
  val complexEventTriThemeDirTrain = conf.root.getString("complex.event-tri.theme.dir.train")
  val complexEventTriProtCauseDirTrain = conf.root.getString("complex.event-tri-prot.cause.dir.train")
  val complexProtTriEventThemeDirTrain = conf.root.getString("complex.prot-tri-event.theme.dir.train")
  val complexEventTriEventCauseDirTrain = conf.root.getString("complex.event-tri-event.cause.dir.train")
  val bindingPairDirTrain = conf.root.getString("binding.pair.dir.train")

  val simpleProtTriThemeDirDev = conf.root.getString("simple.prot-tri.theme.dir.dev")
  val simpleProtTriProtCauseDirDev = conf.root.getString("simple.prot-tri-prot.cause.dir.dev")
  val complexEventTriThemeDirDev = conf.root.getString("complex.event-tri.theme.dir.dev")
  val complexEventTriProtCauseDirDev = conf.root.getString("complex.event-tri-prot.cause.dir.dev")
  val complexProtTriEventThemeDirDev = conf.root.getString("complex.prot-tri-event.theme.dir.dev")
  val complexEventTriEventCauseDirDev = conf.root.getString("complex.event-tri-event.cause.dir.dev")
  val bindingPairDirDev = conf.root.getString("binding.pair.dir.dev")
  
  println("Train corpus stats:")
  datasetBuilder.reportStats(dataSourceDir = trainDir)
  println("Dev corpus stats:")
  datasetBuilder.reportStats(dataSourceDir = devDir)

}