/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.app

import bioee.components.BioNLPSimpleTokenizerBasedDatasetBuilder
import bioee.utils.Conf
import bioee.dataset.generation.BioNLPExampleBuilderComponent
import bioee.text.preprocessing.BioNLPPreprocessingComponent
import bioee.components.BioNLPSimpleTokenizerBasedDatasetBuilder
import bioee.utils.BioNLPConstants
import bioee.domains.Dataset
import com.typesafe.scalalogging.slf4j.LazyLogging

object BioNLPSimpleTokenizerBasedApp extends App with BioNLPSimpleTokenizerBasedDatasetBuilder with LazyLogging {

  val trainDir = conf.root.getString("genia.corpus.train.dir")
  val devDir = conf.root.getString("genia.corpus.dev.dir")
  val testDir = conf.root.getString("genia.corpus.test.dir")

  val simpleProtTriThemeDirTrain = conf.root.getString("simple.prot-tri.theme.dir.train")
  val simpleProtTriProtCauseDirTrain = conf.root.getString("simple.prot-tri-prot.cause.dir.train")
  val complexEventTriThemeDirTrain = conf.root.getString("complex.event-tri.theme.dir.train")
  val complexEventTriProtCauseDirTrain = conf.root.getString("complex.event-tri-prot.cause.dir.train")
  val complexProtTriEventThemeDirTrain = conf.root.getString("complex.prot-tri-event.theme.dir.train")
  val complexEventTriEventCauseDirTrain = conf.root.getString("complex.event-tri-event.cause.dir.train")
  val bindingPairDirTrain = conf.root.getString("binding.pair.dir.train")

  val simpleProtTriThemeDirDev = conf.root.getString("simple.prot-tri.theme.dir.dev")
  val simpleProtTriProtCauseDirDev = conf.root.getString("simple.prot-tri-prot.cause.dir.dev")
  val complexEventTriThemeDirDev = conf.root.getString("complex.event-tri.theme.dir.dev")
  val complexEventTriProtCauseDirDev = conf.root.getString("complex.event-tri-prot.cause.dir.dev")
  val complexProtTriEventThemeDirDev = conf.root.getString("complex.prot-tri-event.theme.dir.dev")
  val complexEventTriEventCauseDirDev = conf.root.getString("complex.event-tri-event.cause.dir.dev")
  val bindingPairDirDev = conf.root.getString("binding.pair.dir.dev")

  val simpleProtTriThemeDatasetTrain: Dataset = Dataset(datasetId = s"${BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI}:${BioNLPConstants.Theme}",
    description = "BioNLP simple events", examples = Seq(), datasetDirPath = simpleProtTriThemeDirTrain,
    negativeLabel = BioNLPConstants.ThemeNone, addNewLabel = true)
  val simpleProtTriProtCauseDatasetTrain = Dataset(datasetId = s"${BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI_PROT}:${BioNLPConstants.Cause}",
    description = "BioNLP simple events", examples = Seq(), datasetDirPath = simpleProtTriProtCauseDirTrain,
    negativeLabel = BioNLPConstants.CauseNone, addNewLabel = true)
  val complexEventTriThemeDatasetTrain = Dataset(datasetId = s"${BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI}:${BioNLPConstants.Theme}",
    description = "BioNLP complex events", examples = Seq(), datasetDirPath = complexEventTriThemeDirTrain,
    negativeLabel = BioNLPConstants.ThemeNone, addNewLabel = true)
  val complexEventTriProtCauseDatasetTrain = Dataset(datasetId = s"${BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_PROT}:${BioNLPConstants.Cause}",
    description = "BioNLP complex events", examples = Seq(), datasetDirPath = complexEventTriProtCauseDirTrain,
    negativeLabel = BioNLPConstants.CauseNone, addNewLabel = true)
  val complexProtTriEventThemeDatasetTrain = Dataset(datasetId = s"${BioNLPConstants.RelationGroups.COMPLEX_REG_PROT_TRI_EVENT}:${BioNLPConstants.Cause}",
    description = "BioNLP complex events", examples = Seq(), datasetDirPath = complexProtTriEventThemeDirTrain,
    negativeLabel = BioNLPConstants.ThemeNone, addNewLabel = true)
  val complexEventTriEventCauseDatasetTrain = Dataset(datasetId = s"${BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_EVENT}:${BioNLPConstants.Cause}",
    description = "BioNLP complex events", examples = Seq(), datasetDirPath = complexEventTriEventCauseDirTrain,
    negativeLabel = BioNLPConstants.CauseNone, addNewLabel = true)
  val bindingPairDatasetTrain = Dataset(datasetId = BioNLPConstants.RelationGroups.BINDING_PAIR,
    description = "BioNLP binding pairs", examples = Seq(), datasetDirPath = bindingPairDirTrain,
    negativeLabel = BioNLPConstants.None, addNewLabel = true)

  logger.info("building train dataset...")

  val datasetsTrain = datasetBuilder.buildFromDir(dataSourceDir = trainDir,
    simpleProtTriThemeDataset = simpleProtTriThemeDatasetTrain,
    simpleProtTriProtCauseDataset = simpleProtTriProtCauseDatasetTrain,
    complexEventTriThemeDataset = complexEventTriThemeDatasetTrain,
    complexEventTriProtCauseDataset = complexEventTriProtCauseDatasetTrain,
    complexProtTriEventThemeDataset = complexProtTriEventThemeDatasetTrain,
    complexEventTriEventCauseDataset = complexEventTriEventCauseDatasetTrain,
    bindingPairDataset = bindingPairDatasetTrain)

  datasetsTrain.foreach(kv => kv._2.save(kv._2.datasetDirPath + ".dataset"))

  val simpleProtTriThemeDatasetDev: Dataset = Dataset(datasetId = s"${BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI}:${BioNLPConstants.Theme}",
    description = "BioNLP simple events", examples = Seq(), datasetDirPath = simpleProtTriThemeDirDev,
    negativeLabel = BioNLPConstants.ThemeNone, addNewLabel = false, labelIndex = simpleProtTriThemeDatasetTrain.labelIndex)
  val simpleProtTriProtCauseDatasetDev = Dataset(datasetId = s"${BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI_PROT}:${BioNLPConstants.Cause}",
    description = "BioNLP simple events", examples = Seq(), datasetDirPath = simpleProtTriProtCauseDirDev,
    negativeLabel = BioNLPConstants.CauseNone, addNewLabel = false, labelIndex = simpleProtTriProtCauseDatasetTrain.labelIndex)
  val complexEventTriThemeDatasetDev = Dataset(datasetId = s"${BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI}:${BioNLPConstants.Theme}",
    description = "BioNLP complex events", examples = Seq(), datasetDirPath = complexEventTriThemeDirDev,
    negativeLabel = BioNLPConstants.ThemeNone, addNewLabel = false, labelIndex = complexEventTriThemeDatasetTrain.labelIndex)
  val complexEventTriProtCauseDatasetDev = Dataset(datasetId = s"${BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_PROT}:${BioNLPConstants.Cause}",
    description = "BioNLP complex events", examples = Seq(), datasetDirPath = complexEventTriProtCauseDirDev,
    negativeLabel = BioNLPConstants.CauseNone, addNewLabel = false, labelIndex = complexEventTriProtCauseDatasetTrain.labelIndex)
  val complexProtTriEventThemeDatasetDev = Dataset(datasetId = s"${BioNLPConstants.RelationGroups.COMPLEX_REG_PROT_TRI_EVENT}:${BioNLPConstants.Cause}",
    description = "BioNLP complex events", examples = Seq(), datasetDirPath = complexProtTriEventThemeDirDev,
    negativeLabel = BioNLPConstants.ThemeNone, addNewLabel = false, labelIndex = complexProtTriEventThemeDatasetTrain.labelIndex)
  val complexEventTriEventCauseDatasetDev = Dataset(datasetId = s"${BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_EVENT}:${BioNLPConstants.Cause}",
    description = "BioNLP complex events", examples = Seq(), datasetDirPath = complexEventTriEventCauseDirDev,
    negativeLabel = BioNLPConstants.CauseNone, addNewLabel = false, labelIndex = complexEventTriEventCauseDatasetTrain.labelIndex)
  val bindingPairDatasetDev = Dataset(datasetId = BioNLPConstants.RelationGroups.BINDING_PAIR,
    description = "BioNLP binding pairs", examples = Seq(), datasetDirPath = bindingPairDirDev,
    negativeLabel = BioNLPConstants.None, addNewLabel = false, labelIndex = bindingPairDatasetTrain.labelIndex)

  logger.info("building dev dataset...")

  val datasetsDev = datasetBuilder.buildFromDir(dataSourceDir = devDir,
    simpleProtTriThemeDataset = simpleProtTriThemeDatasetDev,
    simpleProtTriProtCauseDataset = simpleProtTriProtCauseDatasetDev,
    complexEventTriThemeDataset = complexEventTriThemeDatasetDev,
    complexEventTriProtCauseDataset = complexEventTriProtCauseDatasetDev,
    complexProtTriEventThemeDataset = complexProtTriEventThemeDatasetDev,
    complexEventTriEventCauseDataset = complexEventTriEventCauseDatasetDev,
    bindingPairDataset = bindingPairDatasetDev)

  datasetsDev.foreach(kv => kv._2.save(kv._2.datasetDirPath + ".dataset"))
  exampleBuilder.savePositionVector

}