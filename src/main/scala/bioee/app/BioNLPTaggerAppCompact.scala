/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.app

import bioee.components.BioNLPTaggerCompact
import com.typesafe.scalalogging.slf4j.LazyLogging

object BioNLPTaggerAppCompact extends App with BioNLPTaggerCompact with LazyLogging {
  val trainDir = conf.root.getString("genia.corpus.train.dir")
  val devDir = conf.root.getString("genia.corpus.dev.dir")
  val testDir = conf.root.getString("genia.corpus.test.dir")

  eventConstructor.constructFromDir(devDir)
}