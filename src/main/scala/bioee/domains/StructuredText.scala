/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.domains

abstract class StructuredText(var mentions: Seq[Mention] = Seq(),
  var relations: Seq[Relation] = Seq(),
  var events: Seq[Event] = Seq(),
  var example: Option[Example] = None) {

  def createMention(mention: Mention) = {
    mentions = mentions :+ mention
    mention
  }

  def createRelation(relation: Relation) = {
    relations = relations :+ relation
    relation
  }

  def createEvent(event: Event) = {
    events = events :+ event
    event
  }

  def getMentionById(mentionId: String) = {
    mentions.filter(_.mentionId.getOrElse("") == mentionId).headOption
  }

  def getMentionByTrueLabel(trueLabel: String) = {
    mentions.filter(_.mentionLabel.trueValue.getOrElse("") == trueLabel)
  }

  def relationExists(token1: Token, token2: Token, relationsCheck: Seq[Relation] = relations) = {
    relationsCheck.exists { relation =>
      ((relation.centerMention.head.start == token1.start && relation.centerMention.head.end == token1.end) &&
        (relation.argumentMention.head.start == token2.start && relation.argumentMention.head.end == token2.end)) ||
        ((relation.centerMention.head.start == token2.start && relation.centerMention.head.end == token2.end) &&
          (relation.argumentMention.head.start == token1.start && relation.argumentMention.head.end == token1.end))
    }
  }
}