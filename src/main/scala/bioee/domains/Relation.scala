/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.domains

case class Relation(var eventId: Option[String] = None, var relationId: String, var relationType: Label,
  var centerMention: Mention, var argumentMention: Mention, var argumentRole: Label,
  var example: Option[Example] = None, var groupId: Set[String],
  var childId: Option[String]) {

  override def toString = {
    relationId + "   " +
      centerMention.head.word.trim() + "   " +
      argumentMention.head.word.trim() + "   " +
      argumentRole.trueValue + "   " +
      relationType.trueValue
  }
}