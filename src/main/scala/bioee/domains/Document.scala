/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.domains

case class Document(var docId: String, var text: String, var sentences: Seq[Sentence] = Seq(),
  var tokens: Seq[Token] = Seq(), var textSourceFile: Option[String] = None, 
  var tagSourceFile: Map[String, String] = Map())
  extends StructuredText {

  def tokenAt(offset: Int) = {
    tokens.filter(token => token.start <= offset && token.end >= offset).headOption
  }

//  def sentenceAt(offset: Int) = {
//    sentences.getOrElse(Seq()).filter(sent => sent.start <= offset && sent.end >= offset).headOption
//  }

  def sentenceAt(token: Token): Option[Sentence] = {
    for (sent <- sentences; t <- sent.tokens) {
      if (t.start == token.start && t.end == token.end) {
        return Some(sent)
      }
    }
    None
  }

}