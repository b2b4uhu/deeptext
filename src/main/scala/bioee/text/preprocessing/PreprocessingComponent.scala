/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.text.preprocessing

import bioee.domains._

trait PreprocessingComponent {
  val tokenizer: Tokenizer
  val mentionTaggger: MentionLoader
  val relationLoader: RelationLoader
  val sentenceSplitter: SentenceSplitter

  trait Preprocessor {
    def annotate(doc: Document): Document
    
    def annotate(sentence: Sentence): Sentence
  }

  trait Tokenizer extends Preprocessor
  
  trait MentionLoader extends Preprocessor

  trait SentenceSplitter extends Preprocessor
  
  trait RelationLoader extends Preprocessor


}