/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.text.postprocessing

import bioee.domains.Document
import bioee.utils.BioNLPUtils
import bioee.utils.BioNLPConstants
import java.io.PrintWriter
import java.io.File

trait BioNLPRelationWriterComponent extends RelationWriterComponent {

  class BioNLPRelationWriter(dir: String) extends RelationWriter {

    def save(doc: Document) = {
      val relations = doc.relations
      val out = new PrintWriter(new File(dir, doc.docId + ".a2"), "UTF-8")
      val maxProtId = doc.getMentionByTrueLabel(BioNLPConstants.Protein).map(_.mentionId.get.replace("T", "").toInt).sorted.reverse.headOption.getOrElse(0)
      var triggerId = maxProtId
      val trueRelations = relations.filter { r =>
        val label = r.relationType.predictedValue.getOrElse(r.relationType.trueValue.getOrElse(BioNLPConstants.None))
        label != BioNLPConstants.None
      }
      trueRelations.foreach { r =>
        val label = r.relationType.predictedValue.getOrElse(r.relationType.trueValue.get)
        if (r.centerMention.mentionId.isEmpty) {
          triggerId = triggerId + 1
          r.centerMention.mentionId = Some(s"T$triggerId")
        }
        out.println(s"${r.centerMention.mentionId.get}\t${label} ${r.centerMention.head.start} ${r.centerMention.head.end}\t${r.centerMention.head.word}")
      }
      val eventId = 0
      trueRelations.foreach { r =>

      }
    }
  }

}