/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.text.postprocessing

import bioee.domains.Document

trait RelationWriterComponent {
  val relationWriter: RelationWriter

  trait RelationWriter {
    def save(doc: Document)
  }

}