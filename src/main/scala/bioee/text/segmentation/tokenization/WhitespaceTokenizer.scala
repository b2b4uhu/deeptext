/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.text.segmentation.tokenization

import com.google.common.base.Splitter
import com.google.common.base.CharMatcher
import scala.collection.JavaConversions._

/**
 * Whitespace tokenizer relied on google guava
 */
class WhitespaceTokenizer extends Tokenizer {
  def getTokens(text: String) = {
    Splitter.on(CharMatcher.WHITESPACE).trimResults().omitEmptyStrings().split(text).toList
  }
}