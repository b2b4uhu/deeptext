/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.components

import bioee.dataset.generation.BioNLPExampleBuilderComponent
import bioee.text.preprocessing.BioNLPPreprocessingComponent
import bioee.utils.Conf
import bioee.utils.BioNLPConstants
import bioee.utils.Dictionary
import bioee.utils.StemmedDictionary
import bioee.dataset.generation.BioNLPEventDatasetBuilderComponent
import bioee.dataset.generation.BioNLPEventBuilderComponent

trait BioNLPEventDsBuilder extends BioNLPEventDatasetBuilderComponent
  with BioNLPEventBuilderComponent
  with BioNLPExampleBuilderComponent with BioNLPPreprocessingComponent {

  val conf = new Conf("/home/tsendee/pubmed/tool/BioEE/src/main/resources/application-event.conf")

  val WVPath = conf.root.getString("wv.path")
  val wvLcase = conf.root.getBoolean("wv.lcase")
  val wvLetterNgram = conf.root.getBoolean("wv.letterNgram")
  val wvNonNumeric = conf.root.getBoolean("wv.nonnumeric")
  val indexVectorSize = conf.root.getInt("features.ivector.size")
  val positionVectorPath = conf.root.getString("features.ivector.path")
  val skipAbbrevs = conf.root.getBoolean("preprocessing.loading.skipAbbrevs")
  val dictPathTrigger = conf.root.getString("preprocessing.loading.dic.trigger")

  val a1fileKey: String = BioNLPConstants.a1fileKey
  val a2fileKey: String = BioNLPConstants.a2fileKey
  val argRolesToProcess = BioNLPConstants.roles

  val tokenizer = new SimpleTokenizer
  val mentionTaggger = new BioNLPGeniaMentionLoader(a1fileKey, a2fileKey, skipAbbrevs)
  val relationLoader = new BioNLPGeniaRelationLoader(a2fileKey, argRolesToProcess, false)
  val sentenceSplitter = new CoreNLPSentenceSplitter

  val dic = new StemmedDictionary(dictPathTrigger)
  val eventBuilder = new BioNLPEventBuilder(dic)
  val exampleBuilder = new BioNLPEventExampleBuilder(WVPath, 100, wvLcase, wvNonNumeric, wvLetterNgram)
  val datasetBuilder = new BioNLPEventDatasetBuilder

}