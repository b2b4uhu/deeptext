/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.components

import bioee.dataset.generation.BioNLPExampleBuilderComponent
import bioee.text.preprocessing.BioNLPPreprocessingComponent
import bioee.utils.Conf
import bioee.utils.BioNLPConstants
import bioee.utils.Dictionary
import bioee.utils.StemmedDictionary
import bioee.dataset.generation.BioNLPDatasetBuilderComponent
import bioee.dataset.generation.BioNLPRelationBuilderComponent

trait BioNLPDsBuilderWithTokenFilterOtherFeatures extends BioNLPDatasetBuilderComponent
  with BioNLPRelationBuilderComponent
  with BioNLPExampleBuilderComponent with BioNLPPreprocessingComponent {

  val conf = new Conf("/home/tsendee/pubmed/tool/BioEE/src/main/resources/application-wv200-other-features.conf")

  val WVPath = conf.root.getString("wv.path")
  val wvLcase = conf.root.getBoolean("wv.lcase")
  val wvLetterNgram = conf.root.getBoolean("wv.letterNgram")
  val wvNonNumeric = conf.root.getBoolean("wv.nonnumeric")
  val indexVectorSize = conf.root.getInt("features.ivector.size")
  val positionVectorPath = conf.root.getString("features.ivector.path")
  val skipAbbrevs = conf.root.getBoolean("preprocessing.loading.skipAbbrevs")
  val dictPathTrigger = conf.root.getString("preprocessing.loading.dic.trigger")

  val suffixPath = conf.root.getString("suffix.train.path")
  val brownPath = conf.root.getString("brown.path")
  val brownModelPath = conf.root.getString("brown.model.path")

  val a1fileKey: String = BioNLPConstants.a1fileKey
  val a2fileKey: String = BioNLPConstants.a2fileKey
  val argRolesToProcess = BioNLPConstants.roles

  val tokenizer = new SimpleTokenizer
  val mentionTaggger = new BioNLPGeniaMentionLoader(a1fileKey, a2fileKey, skipAbbrevs)
  val relationLoader = new BioNLPGeniaRelationLoader(a2fileKey, argRolesToProcess)
  val sentenceSplitter = new CoreNLPSentenceSplitter

  val dic = new StemmedDictionary(dictPathTrigger)
  val relationBuilder = new BioNLPGeniaRelationBuilderWithTokenFilter(dic)
  val exampleBuilder = new BioNLPGeniaExampleBuilderWithPositionIndex(WVPath = WVPath,
    maxIndex = 100, wvLcase = wvLcase, wvNonNumeric = wvNonNumeric, wvLetterNgram = wvLetterNgram,
    WVLearn = false, WVVocPath = "", minFreq = 5,
    useOtherFeatures = true, suffixPath = suffixPath,
    brownModelPath = brownModelPath, brownPath2IndexPath = brownPath)
  val datasetBuilder = new BioNLPGeniaDatasetBuilder

}