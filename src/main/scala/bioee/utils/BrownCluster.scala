/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.utils

import java.io.File
import bioee.utils.RichFile.enrichFile
import java.util.StringTokenizer

class BrownCluster(pathsToClusterFile: String, freqThreshold: Int = 1, isLowercaseBrownClusters: Boolean = false) {

  lazy val w2p: Map[String, String] = {
    val f = new File(pathsToClusterFile)
    f.lines.map { line =>
      val st = new StringTokenizer(line)
      val path = st.nextToken()
      val word = st.nextToken()
      val freq = Integer.parseInt(st.nextToken())
      (word, path, freq)
    }.filter(_._3 >= freqThreshold).map(p => p._1 -> p._2).toMap
  }

  def wordPrefix(token: String, n: Int = 20) = {
    val w = if (isLowercaseBrownClusters) token.toLowerCase() else token
    val path = w2p.getOrElse(w, "")
    path.substring(0, math.min(path.length(), n))
  }
}