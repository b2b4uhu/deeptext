/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.utils

import bioee.domains.Token
import java.io.File
import java.io.FilenameFilter
import com.typesafe.scalalogging.slf4j.LazyLogging
import bioee.utils.RichFile.enrichFile
import bioee.domains.Document
import bioee.domains._

object BioNLPUtils extends LazyLogging {

  def normalizeRole(role: String) = {
    if (role.last.isDigit) role.substring(0, role.length - 1) else role
  }

  def fileName(docID: String): String = {
    def orElseLength(n: Int) = if (n > -1) n else docID.length()
    docID.substring(docID.lastIndexOf('/') + 1, orElseLength(docID.lastIndexOf('.')))
  }

  def getFiles(dir: File, postfix: String): Seq[File] = {
    dir.listFiles(new FilenameFilter {
      def accept(dir: File, name: String): Boolean = {
        name.endsWith(postfix)
      }
    })
  }

  def loadDocs(dir: File, filter: Seq[File] => Seq[File] = identity(_),
    maxCount: Int = Int.MaxValue, loadA2File: Boolean = true) = {
    val txtFiles = BioNLPUtils.getFiles(dir, ".txt").take(maxCount)
    val docs = for (txtFile <- filter(txtFiles).toIterator) yield {
      //        logger.info("Loading file %s".format(txtFile.getAbsolutePath))
      val docPath = txtFile.getAbsolutePath
      var slashIndex = docPath.lastIndexOf('/')
      slashIndex = if (slashIndex == -1) 0 else slashIndex
      val filename = docPath.substring(slashIndex)
      val docId = filename.substring(1, filename.indexOf('.'))
      val a1file = (new File(dir, docId + ".a1")).getAbsolutePath()
      val tagSourceFile = if (loadA2File) {
        val a2file = (new File(dir, docId + ".a2")).getAbsolutePath()
        Map(BioNLPConstants.a1fileKey -> a1file,
          BioNLPConstants.a2fileKey -> a2file)
      } else {
        Map(BioNLPConstants.a1fileKey -> a1file)
      }

      val text = txtFile.lines.mkString("\n")
      val normalizedText = text
      val doc = Document(docId = docId, text = normalizedText,
        textSourceFile = Option(filename),
        tagSourceFile = tagSourceFile)
      doc
    }
    docs
  }

  def generateNoneRelation(mentions: Seq[(Mention, Option[String], Option[String])], tokens: Seq[Token], groupId: String, argRole: String, checkRule: (Mention, Token) => Boolean) = {
    for (
      mention <- mentions; token <- tokens if (checkRule(mention._1, token))
    ) yield {
      val centerMention = Mention(mentionId = None, head = token, begin = token,
        end = token)
      val relation = Relation(eventId = mention._3, relationId = mention._1.mentionId.getOrElse("") + ":" + BioNLPConstants.None,
        relationType = Label(trueValue = Option(BioNLPConstants.None)), centerMention = centerMention,
        argumentMention = mention._1, argumentRole = Label(trueValue = Option(argRole)),
        groupId = Set(groupId), childId = mention._2)
      relation
    }
  }

  def generateNoneRelationProt(mentions: Seq[(Mention, Option[String], Option[String])], argMentions: Seq[Mention], groupId: String, argRole: String, checkRule: (Mention, Token) => Boolean) = {
    for (
      mention <- mentions; aMention <- argMentions if (checkRule(mention._1, aMention.head))
    ) yield {
      //      val argumentMention = Mention(mentionId = None, head = token, begin = token,
      //        end = token, sentence = token.sentence, document = token.document)
      val relation = Relation(eventId = mention._3, relationId = aMention.mentionId.getOrElse("") + ":" + BioNLPConstants.None,
        relationType = Label(trueValue = Option(BioNLPConstants.None)), centerMention = mention._1,
        argumentMention = aMention, argumentRole = Label(trueValue = Option(argRole)),
        groupId = Set(groupId), childId = mention._2)
      relation
    }
  }

  def generateNoneRelationProtTri(mentions: Seq[(Mention, Option[String], Option[String])], tokens: Seq[Token], groupId: String, argRole: String, checkRule: (Mention, Token) => Boolean) = {
    for (
      mention <- mentions; token <- tokens if (checkRule(mention._1, token))
    ) yield {
      val argumentMention = Mention(mentionId = None, head = token, begin = token,
        end = token)
      val relation = Relation(eventId = mention._3, relationId = argumentMention.mentionId.getOrElse("") + ":" + BioNLPConstants.None,
        relationType = Label(trueValue = Option(BioNLPConstants.None)), centerMention = mention._1,
        argumentMention = argumentMention, argumentRole = Label(trueValue = Option(argRole)),
        groupId = Set(groupId), childId = mention._2)
      relation
    }
  }

  def sameRelationFilterRule(relations: Seq[Relation], sentence: Sentence)(mention: Mention, token: Token) = {
    !sentence.relationExists(mention.head, token, relations) && !((mention.head.start == token.start) && (mention.head.end == token.end))
  }

  def sameRelationFilterRuleToken(relations: Seq[Relation], sentence: Sentence)(mention: Mention, token: Token) = {
    !((mention.head.start == token.start) && (mention.head.end == token.end))
  }

  def generateNoneEvent(mentions: Seq[(Mention, Option[String], Option[String])], tokens: Seq[Token], groupId: String, argRole: String, checkRule: (Mention, Token) => Boolean) = {
    for (
      mention <- mentions; token <- tokens if (checkRule(mention._1, token))
    ) yield {
      val centerMention = Mention(mentionId = None, head = token, begin = token,
        end = token)
      val relation = Relation(eventId = mention._3, relationId = mention._1.mentionId.getOrElse("") + ":" + BioNLPConstants.None,
        relationType = Label(trueValue = Option(BioNLPConstants.None)), centerMention = centerMention,
        argumentMention = mention._1, argumentRole = Label(trueValue = Option(argRole)),
        groupId = Set(groupId), childId = mention._2)
      relation
    }
  }

}

object BioNLPConstants {
  val Binding = "Binding"

  val Transcription = "Transcription"

  val ProteinCatabolism = "Protein_catabolism"

  val Localization = "Localization"

  val None = "None"

  val GeneExpression = "Gene_expression"

  val Regulation = "Regulation"

  val PositiveRegulation = "Positive_regulation"

  val NegativeRegulation = "Negative_regulation"

  val Phosphorylation = "Phosphorylation"

  val Theme = "Theme"
  val Theme2 = "Theme2"

  val Cause = "Cause"

  val Participant = "Participant"

  val NoSite = "NoSite"

  val Entity = "Entity"
  val EntityTag = "entity"

  val Protein = "Protein"
  val SameBinding = "SameBinding"

  val a1fileKey: String = "a1file"
  val a2fileKey: String = "a2file"

  val ThemeNone = "Theme:None"
  val CauseNone = "Cause:None"
  val CauseComplex = "Complex"

  val types = Seq(Binding, Transcription, ProteinCatabolism, Localization,
    GeneExpression, Regulation, PositiveRegulation, NegativeRegulation, Phosphorylation, None)

  val roles = Set(None, Theme, Cause, Theme2)

  def isComplex(lbl: String) = Regulation == lbl || PositiveRegulation == lbl || NegativeRegulation == lbl
  def isSimple(lbl: String) = {
    !isComplex(lbl) && lbl != Binding && lbl != SameBinding && lbl != None && lbl != CauseComplex
  }

  def isSimpleWithBinding(lbl: String) = {
    !isComplex(lbl) && lbl != SameBinding && lbl != None && lbl != CauseComplex
  }

  def hasCauseArg(event: Event) = {
    event.arguments.size > 1 || !event.childCause.isEmpty || (!event.childTheme.isEmpty && event.arguments.size > 0)
  }

  object RelationGroups {
    val SIMPLE_EVENT = "simple"
    val COMPLEX_REG = "complex"
    val BINDING_PAIR = "binding"
    val SIMPLE_EVENT_PROT_TRI = "simple_prot_tri"
    val SIMPLE_EVENT_PROT_TRI_PROT = "simple_prot_tri_prot"
    val COMPLEX_REG_EVENT_TRI = "complex_event_tri"
    val COMPLEX_REG_EVENT_TRI_PROT = "complex_event_tri_prot"
    val COMPLEX_REG_PROT_TRI_EVENT = "complex_prot_tri_event"
    val COMPLEX_REG_EVENT_TRI_EVENT = "complex_event_tri_event"
  }

}