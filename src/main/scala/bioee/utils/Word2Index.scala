/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.utils

import java.io.File
import bioee.utils.RichFile.enrichFile

class Word2Index() {
  var t2i: Map[String, Int] = null

  def loadVoc(voc: String, minFreq: Int = 5) = {
    val w2f = new File(voc)
    t2i = w2f.lines.toList.map(l => l.split("\t")).filter(_(1).toInt > minFreq).map(_(0)).zipWithIndex.toMap
  }

  def toIndex(token: String) = {
    t2i.getOrElse(token, t2i(TextHelpers.UNKNOWN))
  }
}