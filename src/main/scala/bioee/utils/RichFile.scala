/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.utils

import java.io._
import scala.io._
import scala.collection.Iterator
import java.nio.charset.StandardCharsets
import java.nio.charset.CodingErrorAction

class RichFile(file: File) {
  
  implicit val decoder = StandardCharsets.UTF_8.newDecoder()
  decoder.onMalformedInput(CodingErrorAction.IGNORE)

  def text = Source.fromFile(file)(decoder).mkString

  def lines = Source.fromFile(file)(decoder).getLines

  def text_=(s: String) {
    val out = new PrintWriter(file, "UTF-8")
    try { out.print(s) }
    finally { out.close }
  }

  def lines_=(ls: Iterator[String]) {
    val out = new PrintWriter(file, "UTF-8")
    try { ls.foreach(out.println(_)) }
    finally { out.close }
  }
  
}

object RichFile {

  implicit def enrichFile(file: File) = new RichFile(file)
  
  def recursiveListFiles(f: File): Array[File] = {
    val these = f.listFiles
    these ++ these.filter(_.isDirectory).flatMap(recursiveListFiles _)
  }
  
  def childFiles(f: File, suffix: String) = {
    recursiveListFiles(f).filter(_.getName.endsWith(suffix))
  }

}