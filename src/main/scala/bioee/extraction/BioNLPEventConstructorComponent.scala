/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.extraction

import bioee.domains._
import bioee.utils.BioNLPConstants
import bioee.utils.BioNLPUtils
import java.io.File
import bioee.utils.RichFile._

trait BioNLPEventConstructorComponent extends EventConstructorComponent {
  this: ExtractionComponent =>

  class BioNLPEventConstructor(outputDir: String) extends EventConstructor {
    val outDir = new File(outputDir)

    def construct(doc: Document): Document = {
      relationExtractor.extract(doc)
      var mention2tId: Map[String, String] = Map()
      var tId = doc.mentions.size

      def mentionId(k: String) = {
        mention2tId.getOrElse(k, {
          tId = tId + 1
          mention2tId = mention2tId + (k -> tId.toString)
          tId.toString
        })
      }
      var eId = 0

      doc.sentences.foreach { sent =>
        val relations = sent.relations
        /**
         * Simple event construction
         */
        relations.filter(r => BioNLPConstants.isSimple(r.relationType.predictedValue.get)).foreach { r =>
          val trigger = r.centerMention.copy()
          val k = trigger.head.indexInDocument
          val id = mentionId(r.relationType.predictedValue.get + k)
          trigger.mentionId = Some(id)
          trigger.mentionLabel = r.relationType
          eId = eId + 1
          val rMention = RoleMention(r.argumentRole, r.argumentMention)
          val event = Event(id = eId.toString, eventType = r.relationType, trigger = trigger,
            arguments = Seq(rMention), relations = Seq(r))
          sent.createEvent(event)
          doc.createEvent(event)
        }
        /**
         * Binding event construction
         */
        val bindingRelations = relations.filter(_.relationType.predictedValue.get == BioNLPConstants.Binding).
          map { r =>
            val k = r.argumentMention.head.indexInDocument
            (k -> r)
          }.toMap
        val sameBindings = relations.filter(_.relationType.predictedValue.get == BioNLPConstants.SameBinding).
          foldLeft(Map[Int, Set[Int]]()) { (m, r) =>
            val k = r.centerMention.head.indexInDocument
            val k1 = r.argumentMention.head.indexInDocument
            m + (k -> (m.getOrElse(k, Set()) + k1), (k1 -> (m.getOrElse(k1, Set()) + k)))
          }
        var seen = Set[Int]()
        bindingRelations.foreach { pair =>
          val (aId, r) = pair
          if (!seen(aId)) {
            val aIds = sameBindings.getOrElse(aId, Set())
            seen = seen + aId ++ aIds
            val trigger = r.centerMention.copy()
            val k = trigger.head.indexInDocument
            val id = mentionId(r.relationType.predictedValue.get + k)
            trigger.mentionId = Some(id)
            trigger.mentionLabel = r.relationType
            val rs = (aIds + aId).map { aId =>
              val r = bindingRelations(aId)
              r
            }.toSeq
            val rMention = rs.map { r =>
              RoleMention(r.argumentRole, r.argumentMention)
            }
            eId = eId + 1
            val event = Event(id = eId.toString, eventType = r.relationType, trigger = trigger,
              arguments = rMention, relations = rs)
            sent.createEvent(event)
            doc.createEvent(event)
          }
        }
        /**
         * Complex event construction
         */
        val complexRelations = relations.filter(r => BioNLPConstants.isComplex(r.relationType.predictedValue.get))
        val causeArgs = relations.filter(_.relationType.predictedValue.get == BioNLPConstants.CauseComplex).
          foldLeft(Map[String, Set[Relation]]()) { (m, r) =>
            val k = r.centerMention.head.indexInDocument
            val k1 = r.argumentMention.head.indexInDocument
            m + (s"$k:$k1" -> (m.getOrElse(s"$k:$k1", Set()) + r))
          }
          .map { z =>
            z._1.split(":")(0).toInt -> z._2
          }
        var added = Set[String]()
        complexRelations.foreach { r =>
          val rMention = RoleMention(r.argumentRole, r.argumentMention)
          val k = r.centerMention.head.indexInDocument
          val r1s = causeArgs.get(k)
          if (r1s.isEmpty) {
            val trigger = r.centerMention.copy()
            val k = trigger.head.indexInDocument
            val id = mentionId(r.relationType.predictedValue.get + k)
            trigger.mentionId = Some(id)
            trigger.mentionLabel = r.relationType
            val eStr = s"${r.relationType.predictedValue.get}:${trigger.mentionId.get}:${r.argumentRole.predictedValue.get}:${r.argumentMention.mentionId.get}"
            if (!added(eStr)) {
              eId = eId + 1
              val event = Event(id = eId.toString, eventType = r.relationType, trigger = trigger,
                arguments = Seq(rMention), relations = Seq(r))
              sent.createEvent(event)
              doc.createEvent(event)
              added = added + eStr
            }
          } else {
            r1s.get.foreach { r1 =>
              val trigger = r.centerMention.copy()
              val k = trigger.head.indexInDocument
              val id = mentionId(r.relationType.predictedValue.get + k)
              trigger.mentionId = Some(id)
              trigger.mentionLabel = r.relationType
              val rm = RoleMention(r1.argumentRole, r1.argumentMention)
              val eStr = s"${r.relationType.predictedValue.get}:${trigger.mentionId.get}:${r.argumentRole.predictedValue.get}:${r.argumentMention.mentionId.get}:${r1.argumentRole.predictedValue.get}:${r1.argumentMention.mentionId.get}"
              if (!added(eStr)) {
                eId = eId + 1
                val event = Event(id = eId.toString, eventType = r.relationType, trigger = trigger,
                  arguments = Seq(rMention, rm), relations = Seq(r, r1))
                sent.createEvent(event)
                doc.createEvent(event)
                added = added + eStr
              }
            }
          }
        }
      }
      doc
    }

    def constructFromDir(dataSourceDir: String, maxDocs: Int = Int.MaxValue) = {
      val dataDir = new File(dataSourceDir)
      val docs = BioNLPUtils.loadDocs(dir = dataDir, maxCount = maxDocs, loadA2File = false)
      docs.foreach { doc =>
        val a2file = new File(outDir, doc.docId + ".a2")
        construct(doc)
        val events = doc.events
        val triStr = events.map { e =>
          e.trigger.mentionId.get -> s"T${e.trigger.mentionId.get}\t${e.eventType.predictedValue.get} ${e.trigger.head.start} ${e.trigger.head.end}\t${e.trigger.head.word}"
        }.toMap.map(_._2)
        val eStr = events.map { e =>
          val ros = e.arguments
          var themeStr = ""
          var tC = 1
          var cC = 1
          var causeStr = ""
          for (t <- ros) {
            val role = t.role.predictedValue.get
            val rId = t.argument.mentionId.get
            role match {
              case BioNLPConstants.Theme =>
                themeStr = if (themeStr.length == 0)
                  s"$role:$rId"
                else {
                  tC = tC + 1
                  themeStr + s" $role$tC:$rId"
                }
              case BioNLPConstants.Cause =>
                causeStr = if (causeStr.length == 0) s"$role:$rId"
                else {
                  cC = cC + 1
                  causeStr + s" $role$cC:$rId"
                }
              case _ =>
            }
          }
          themeStr = if (causeStr.length() > 0) s"$themeStr $causeStr" else themeStr
          s"E${e.id}\t${e.eventType.predictedValue.get}:T${e.trigger.mentionId.get} $themeStr"
        }
        a2file.lines = (triStr ++ eStr).toIterator
      }
    }

  }

  class BioNLPEventConstructorCompact(outputDir: String) extends EventConstructor {
    val outDir = new File(outputDir)

    def construct(doc: Document): Document = {
      relationExtractor.extract(doc)
      var mention2tId: Map[String, String] = Map()
      var tId = doc.mentions.size

      def mentionId(k: String) = {
        mention2tId.getOrElse(k, {
          tId = tId + 1
          mention2tId = mention2tId + (k -> tId.toString)
          tId.toString
        })
      }
      var eId = 0

      doc.sentences.foreach { sent =>
        var tri2EId: Map[Int, Set[String]] = Map()
        val relations = sent.relations
        /**
         * Simple event construction
         */
        relations.filter(r => BioNLPConstants.isSimple(r.relationType.predictedValue.get)).foreach { r =>
          val trigger = r.centerMention.copy()
          val k = trigger.head.indexInDocument
          val id = mentionId(r.relationType.predictedValue.get + k)
          trigger.mentionId = Some("T" + id)
          trigger.mentionLabel = r.relationType
          val rMention = RoleMention(r.argumentRole, r.argumentMention)
          eId = eId + 1
          val eIdStr = "E" + eId.toString
          val event = Event(id = eIdStr, eventType = r.relationType, trigger = trigger,
            arguments = Seq(rMention), relations = Seq(r))
          sent.createEvent(event)
          doc.createEvent(event)
          tri2EId = tri2EId + (k -> (tri2EId.getOrElse(k, Set()) + eIdStr))
        }
        /**
         * Binding event construction
         */
        val bindingRelations = relations.filter(_.relationType.predictedValue.get == BioNLPConstants.Binding)
        val sameBindings = relations.filter(_.relationType.predictedValue.get == BioNLPConstants.SameBinding).
          foldLeft(Map[Int, Set[Int]]()) { (m, r) =>
            val k = r.centerMention.head.indexInDocument
            val k1 = r.argumentMention.head.indexInDocument
            m + (k -> (m.getOrElse(k, Set()) + k1), (k1 -> (m.getOrElse(k1, Set()) + k)))
          }
        val bindingRelationsSet = bindingRelations.foldLeft(Map[String, Set[Relation]]()) { (m, r) =>
          val cId = r.centerMention.head.indexInDocument
          val aId = r.argumentMention.head.indexInDocument
          val k = s"$cId:$aId"
          m + (k -> (m.getOrElse(k, Set()) + r))
        }
        var seen = Set[String]()
        bindingRelations.foreach { r =>
          val cId = r.centerMention.head.indexInDocument
          val aId = r.argumentMention.head.indexInDocument
          val seenId = s"$cId:$aId"
          if (!seen(seenId)) {
            val aIds = sameBindings.getOrElse(aId, Set()).map(aId => s"$cId:$aId") + seenId
            seen = seen ++ aIds
            val trigger = r.centerMention.copy()
            val k = trigger.head.indexInDocument
            val id = mentionId(r.relationType.predictedValue.get + k)
            trigger.mentionId = Some("T" + id)
            trigger.mentionLabel = r.relationType
            val rs = aIds.flatMap { aId =>
              val r = bindingRelationsSet.getOrElse(aId, Set())
              r
            }.toSeq
            val rMention = rs.map { r =>
              RoleMention(r.argumentRole, r.argumentMention)
            }
            eId = eId + 1
            val eIdStr = "E" + eId.toString
            val event = Event(id = eIdStr, eventType = r.relationType, trigger = trigger,
              arguments = rMention, relations = rs)
            sent.createEvent(event)
            doc.createEvent(event)
            tri2EId = tri2EId + (k -> (tri2EId.getOrElse(k, Set()) + eIdStr))
          }
        }
        /**
         * Complex event construction
         */
        val complexRelations = relations.filter(r => BioNLPConstants.isComplex(r.relationType.predictedValue.get))
        val causeArgs = relations.filter(_.relationType.predictedValue.get == BioNLPConstants.CauseComplex).
          foldLeft(Map[String, Set[Relation]]()) { (m, r) =>
            val k = r.centerMention.head.indexInDocument
            val k1 = r.argumentMention.head.indexInDocument
            m + (s"$k:$k1" -> (m.getOrElse(s"$k:$k1", Set()) + r))
          }
          .map { z =>
            z._1.split(":")(0).toInt -> z._2
          }
        var added = Set[String]()
        complexRelations.foreach { r =>
          val argMention = r.argumentMention
          val argLoc = argMention.head.indexInDocument
          val mayEIds = tri2EId.get(argLoc)
          val rMentions = if (!mayEIds.isEmpty) {
            mayEIds.get.map { eId =>
              val eArg = argMention.copy()
              eArg.mentionId = Some(eId)
              RoleMention(r.argumentRole, eArg)
            }
          } else {
            Set(RoleMention(r.argumentRole, r.argumentMention.copy()))
          }
          val triLoc = r.centerMention.head.indexInDocument
          rMentions.foreach { rMention =>
            val r1s = causeArgs.get(triLoc)
            if (r1s.isEmpty) {
              val trigger = r.centerMention.copy()
              val id = mentionId(r.relationType.predictedValue.get + triLoc)
              trigger.mentionId = Some("T" + id)
              trigger.mentionLabel = r.relationType
              r.relationType.predictedValue.get
              trigger.mentionId.get
              rMention.role.predictedValue.get
              rMention.argument.mentionId.get
              val eStr = s"${r.relationType.predictedValue.get}:${trigger.mentionId.get}:${rMention.role.predictedValue.get}:${rMention.argument.mentionId.get}"
              if (!added(eStr)) {
                eId = eId + 1
                val eIdStr = "E" + eId.toString
                val event = Event(id = eIdStr, eventType = r.relationType, trigger = trigger,
                  arguments = Seq(rMention), relations = Seq(r))
                sent.createEvent(event)
                doc.createEvent(event)
                added = added + eStr
              }
            } else {
              r1s.get.foreach { r1 =>
                val argMention1 = r1.argumentMention
                val mayEIds1 = tri2EId.get(argMention1.head.indexInDocument)
                val rMentions1 = if (!mayEIds1.isEmpty) {
                  mayEIds1.get.map { eId =>
                    val eArg = argMention.copy()
                    eArg.mentionId = Some(eId)
                    RoleMention(r1.argumentRole, eArg)
                  }
                } else {
                  Set(RoleMention(r1.argumentRole, r1.argumentMention.copy()))
                }
                rMentions1.foreach { rMention1 =>
                  val trigger = r.centerMention.copy()
                  val id = mentionId(r.relationType.predictedValue.get + triLoc)
                  trigger.mentionId = Some("T" + id)
                  trigger.mentionLabel = r.relationType
                  r.relationType.predictedValue.get
                  trigger.mentionId.get
                  rMention.role.predictedValue.get
                  rMention.argument.mentionId.get
                  rMention1.role.predictedValue.get
                  rMention1.argument.mentionId.get
                  val eStr = s"${r.relationType.predictedValue.get}:${trigger.mentionId.get}:${rMention.role.predictedValue.get}:${rMention.argument.mentionId.get}:${rMention1.role.predictedValue.get}:${rMention1.argument.mentionId.get}"
                  if (!added(eStr)) {
                    eId = eId + 1
                    val eIdStr = "E" + eId.toString
                    val event = Event(id = eIdStr, eventType = r.relationType, trigger = trigger,
                      arguments = Seq(rMention, rMention1), relations = Seq(r, r1))
                    sent.createEvent(event)
                    doc.createEvent(event)
                    added = added + eStr
                  }
                }
              }
            }
          }
        }
      }
      doc
    }

    def constructFromDir(dataSourceDir: String, maxDocs: Int = Int.MaxValue) = {
      val dataDir = new File(dataSourceDir)
      val docs = BioNLPUtils.loadDocs(dir = dataDir, maxCount = maxDocs, loadA2File = false)
      docs.foreach { doc =>
        val a2file = new File(outDir, doc.docId + ".a2")
        construct(doc)
        val events = doc.events
        val triStr = events.map { e =>
          e.trigger.mentionId.get -> s"${e.trigger.mentionId.get}\t${e.eventType.predictedValue.get} ${e.trigger.head.start} ${e.trigger.head.end}\t${e.trigger.head.word}"
        }.toMap.map(_._2)
        val eStr = events.map { e =>
          val ros = e.arguments
          var themeStr = ""
          var tC = 1
          var cC = 1
          var causeStr = ""
          for (t <- ros) {
            val role = t.role.predictedValue.get
            val rId = t.argument.mentionId.get
            role match {
              case BioNLPConstants.Theme =>
                themeStr = if (themeStr.length == 0)
                  s"$role:$rId"
                else {
                  tC = tC + 1
                  themeStr + s" $role$tC:$rId"
                }
              case BioNLPConstants.Cause =>
                causeStr = if (causeStr.length == 0) s"$role:$rId"
                else {
                  cC = cC + 1
                  causeStr + s" $role$cC:$rId"
                }
              case _ =>
            }
          }
          themeStr = if (causeStr.length() > 0) s"$themeStr $causeStr" else themeStr
          s"${e.id}\t${e.eventType.predictedValue.get}:${e.trigger.mentionId.get} $themeStr"
        }
        a2file.lines = (triStr ++ eStr).toIterator
      }
    }

  }
}