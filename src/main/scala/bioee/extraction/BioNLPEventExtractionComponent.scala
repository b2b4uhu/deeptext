/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.extraction

import bioee.dataset.generation.ExampleBuilderComponent
import bioee.domains._
import bioee.text.preprocessing.PreprocessingComponent
import bioee.utils.Dictionary
import bioee.utils._
import bioee.utils.BioNLPUtils._
import java.io.File
import bioee.utils.RichFile._
import scala.sys.process._
import BioNLPConstants.RelationGroups._
import scala.util.control.Breaks._

trait BioNLPEventExtractionComponent extends ExtractionComponent {
  this: ExampleBuilderComponent with PreprocessingComponent =>

  class BioNLPGeniaEventExtractor(triDic: Dictionary) extends RelationExtractor {

    def extract(doc: Document): Document = {
      tokenizer.annotate(doc)
      sentenceSplitter.annotate(doc)
      mentionTaggger.annotate(doc)
      var eNoneId = 0
      doc.sentences.foreach { sent =>
        val protMentions = sent.getMentionByTrueLabel(BioNLPConstants.Protein)
        val triggerCandidates = sent.tokens.filter(token => triDic.exists(token.word))
        //        val checkRule = sameRelationFilterRuleToken(Seq(), sent)_
        //        for (protMen <- protMentions; tri <- triggerCandidates if checkRule(protMen, tri)) {
        for (protMen <- protMentions; tri <- triggerCandidates) {
          val triMention = Mention(mentionId = None, head = tri, begin = tri,
            end = tri)
          val themeArg = RoleMention(role = Label(trueValue = Option(BioNLPConstants.ThemeNone)), argument = protMen)
          val event = Event(id = eNoneId.toString, eventType = Label(trueValue = Option(BioNLPConstants.None)),
            trigger = triMention, arguments = Seq(themeArg),
            relations = Seq(), groupId = Set(SIMPLE_EVENT_PROT_TRI))
          sent.createEvent(event)
          doc.createEvent(event)
          eNoneId += 1
        }
      }
      exampleBuilder.build(doc)
      relationAnnotater(SIMPLE_EVENT_PROT_TRI).extract(doc)
      // remove none events and keep only real
      doc.events = doc.events.filter(_.eventType.predictedValue.get != BioNLPConstants.None)
      doc.sentences.foreach { sent =>
        sent.events = sent.events.filter(_.eventType.predictedValue.get != BioNLPConstants.None)
      }
      var eId2eId = Map[String, String]()
      doc.sentences.foreach { sent =>
        val events = sent.events
        val bindings = events.filter(_.eventType.predictedValue.get == BioNLPConstants.Binding)
        val bindingThemes = bindings.map(_.arguments(0).argument.mentionId.get).toSet
        val protMentions = sent.getMentionByTrueLabel(BioNLPConstants.Protein).filterNot(m => bindingThemes(m.mentionId.get))
        for (
          binding <- bindings; protMen <- protMentions
        ) {
          val theme2Arg = RoleMention(role = Label(trueValue = Option(BioNLPConstants.ThemeNone)), argument = protMen)
          val event = binding.copy(id = eNoneId.toString, arguments = binding.arguments :+ theme2Arg,
            eventType = Label(trueValue = Option(BioNLPConstants.None)),
            exampleTheme = None, exampleCause = None, groupId = binding.groupId + SIMPLE_EVENT_PROT_TRI_PROT)
          eId2eId = eId2eId + (eNoneId.toString -> binding.id)
          sent.createEvent(event)
          doc.createEvent(event)
          eNoneId += 1
        }
      }
      exampleBuilder.build(doc)
      relationAnnotater(SIMPLE_EVENT_PROT_TRI_PROT).extract(doc)
      doc.events = doc.events.filter(_.eventType.predictedValue.get != BioNLPConstants.None)
      doc.sentences.foreach { sent =>
        sent.events = sent.events.filter(_.eventType.predictedValue.get != BioNLPConstants.None)
      }
      // pruning the old events that created new events with additional argument
      val ids2Remove = doc.events.flatMap { e =>
        eId2eId.get(e.id)
      }.toSet
      doc.events = doc.events.filterNot(e => ids2Remove(e.id))
      doc.sentences.foreach { sent =>
        sent.events = sent.events.filterNot(e => ids2Remove(e.id))
      }
      var c = 0
      var lastCauseId = 0
      var lastThemeId = 0
      breakable {
        while (true) {
          // cause completion
          eId2eId = Map[String, String]()
          var tempEId = eNoneId
          doc.sentences.foreach { sent =>
            val regs = sent.events.filter(e => BioNLPConstants.isComplex(e.eventType.predictedValue.get)).filter(_.id.toInt >= lastCauseId)
              .filterNot(e => BioNLPConstants.hasCauseArg(e))
            val protMentions = sent.getMentionByTrueLabel(BioNLPConstants.Protein)
            val pEvents = for (
              reg <- regs; protMen <- protMentions if reg.arguments.size > 0 && reg.arguments(0).argument.mentionId.get != protMen.mentionId.get
            ) yield {
              val causeArg = RoleMention(role = Label(trueValue = Option(BioNLPConstants.CauseNone)), argument = protMen)
              val event = reg.copy(id = eNoneId.toString, arguments = reg.arguments :+ causeArg,
                eventType = Label(trueValue = reg.eventType.predictedValue), childCause = None,
                exampleTheme = None, exampleCause = None, groupId = Set(COMPLEX_REG_EVENT_TRI_PROT))
              eId2eId = eId2eId + (eNoneId.toString -> reg.id)
              eNoneId += 1
              event
            }
            val nEvents = for (reg <- regs; e <- sent.events if reg.id != e.id) yield {
              val event = reg.copy(id = eNoneId.toString, childCause = Some(e),
                eventType = Label(trueValue = reg.eventType.predictedValue),
                exampleTheme = None, exampleCause = None, groupId = Set(COMPLEX_REG_EVENT_TRI_PROT))
              eId2eId = eId2eId + (eNoneId.toString -> reg.id)
              eNoneId += 1
              event
            }
            (pEvents ++ nEvents).foreach { event =>
              sent.createEvent(event)
              doc.createEvent(event)
            }
          }
          lastCauseId = tempEId
          exampleBuilder.build(doc)
          relationAnnotater(BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_PROT).extract(doc)
          doc.events = doc.events.filter(_.eventType.predictedValue.get != BioNLPConstants.None)
          doc.sentences.foreach { sent =>
            sent.events = sent.events.filter(_.eventType.predictedValue.get != BioNLPConstants.None)
          }
          // pruning the old events that created new events with additional argument
          val ids2Remove = doc.events.flatMap { e =>
            eId2eId.get(e.id)
          }.toSet
          doc.events = doc.events.filterNot(e => ids2Remove(e.id))
          doc.sentences.foreach { sent =>
            sent.events = sent.events.filterNot(e => ids2Remove(e.id))
          }
          c += 1
          if (c == 2) break
          // them completion
          tempEId = eNoneId
          val totalEvents = doc.events.size
          doc.sentences.foreach { sent =>
            val triggerCandidates = sent.tokens.filter(token => triDic.exists(token.word))
            for (
              e <- sent.events.filter(_.id.toInt >= lastThemeId); tri <- triggerCandidates if e.trigger.head.start != tri.start || e.trigger.head.end != tri.end
            ) {
              val triMention = Mention(mentionId = None, head = tri, begin = tri,
                end = tri)
              val event = Event(id = eNoneId.toString, eventType = Label(trueValue = Option(BioNLPConstants.None)),
                trigger = triMention, arguments = Seq(), childTheme = Some(e),
                relations = Seq(), groupId = Set(COMPLEX_REG_EVENT_TRI))
              sent.createEvent(event)
              doc.createEvent(event)
              eNoneId += 1
            }
          }
          lastThemeId = tempEId
          exampleBuilder.build(doc)
          relationAnnotater(BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI).extract(doc)
          doc.events = doc.events.filter(_.eventType.predictedValue.get != BioNLPConstants.None)
          doc.sentences.foreach { sent =>
            sent.events = sent.events.filter(_.eventType.predictedValue.get != BioNLPConstants.None)
          }
          if (totalEvents == doc.events.size) break
        }
      }
      doc
    }
  }

  class BioNLPGeniaEventAnnotater(rGroup: String, dataDir: String, classifierPath: String, indexAndLabel: String) extends RelationAnnotater {
    val index2Label = indexAndLabel.split("\t").map { l =>
      val ls = l.split(" -> ")
      (ls(1).trim.toInt, ls(0).trim)
    }.toMap

    def extract(doc: Document): Document = {
      val docDir = new File(dataDir + doc.docId)
      if (!docDir.exists()) docDir.mkdir()
      val sentDir = new File(dataDir + doc.docId + "/sent")
      if (!sentDir.exists()) sentDir.mkdir()
      val exampleList = new File(dataDir + doc.docId + "/example.list." + rGroup)
      var paths = Seq[String]()
      var events = Seq[Event]()
      doc.sentences.foreach { sent =>
        val sentPath = dataDir + doc.docId + "/sent/" + sent.indexInDocument
        if (!new File(sentPath).exists()) sent.saveExample(path = sentPath)
        val es = sent.events.filter(e => e.groupId(rGroup) && e.eventType.predictedValue.isEmpty)
        val ps = es.map { e =>
          val example = rGroup match {
            case SIMPLE_EVENT_PROT_TRI =>
              e.exampleTheme.get
            case SIMPLE_EVENT_PROT_TRI_PROT =>
              e.exampleCause.get
            case COMPLEX_REG_EVENT_TRI =>
              e.exampleTheme.get
            case COMPLEX_REG_EVENT_TRI_PROT =>
              e.exampleCause.get
          }
          val pathToSave = dataDir + doc.docId + "/" + e.id + "." + rGroup + ".0"
          example.saveEvent(path = pathToSave, docId = doc.docId, e = e, sentPath = sent.indexInDocument.toString)
          pathToSave
        }
        paths = paths ++ ps
        events = events ++ es
      }
      exampleList.lines = paths.toIterator
      s"/usr/bin/python $classifierPath --dir $dataDir/${doc.docId} --group $rGroup".!!
      val labels = new File(dataDir + doc.docId + "/label.list." + rGroup).lines.filter(_ != "").map { l =>
        index2Label(l.toInt)
      }.toIndexedSeq
      events.zipWithIndex.foreach { pair =>
        val (event, i) = pair
        val lbls = labels(i).split(":")
        if (lbls(0) == BioNLPConstants.CauseComplex)
          event.eventType = Label(predictedValue = event.eventType.trueValue)
        else
          event.eventType = Label(predictedValue = Some(lbls(0)))
      }
      doc
    }
  }

}