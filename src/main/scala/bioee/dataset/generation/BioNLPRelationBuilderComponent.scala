/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.dataset.generation

import bioee.utils.Dictionary
import bioee.domains._
import bioee.utils.BioNLPUtils._
import bioee.utils.BioNLPConstants

trait BioNLPRelationBuilderComponent extends RelationBuilderComponent {

  class BioNLPGeniaRelationBuilder extends RelationBuilder {

    def build(doc: Document) = {
      doc.sentences.foreach { sent =>
        val entityMentionChildIds = sent.getMentionByTrueLabel(BioNLPConstants.Protein).map((_, None))
        generateNoneRelation(entityMentionChildIds, sent, BioNLPConstants.RelationGroups.SIMPLE_EVENT)
          .foreach { relation =>
            sent.createRelation(relation)
            doc.createRelation(relation)
          }

        val triggerMentionsChildIds = sent.relations.filter(_.groupId(BioNLPConstants.RelationGroups.SIMPLE_EVENT))
          .map(r => (r.centerMention, Some(r.relationId)))
        generateNoneRelation(triggerMentionsChildIds, sent, BioNLPConstants.RelationGroups.COMPLEX_REG)
          .foreach { relation =>
            sent.createRelation(relation)
            doc.createRelation(relation)
          }
        val bindingRelations = sent.relations
          .filter(relation => relation.centerMention.mentionLabel.trueValue.getOrElse("") == BioNLPConstants.Binding).toIndexedSeq
        for (i <- 0 until bindingRelations.length; j <- i + 1 until bindingRelations.length) {
          val centerMention = bindingRelations(i).argumentMention
          val argMention = bindingRelations(j).argumentMention
          val labelValue = if (bindingRelations(i).relationId == bindingRelations(j).relationId) {
            BioNLPConstants.SameBinding
          } else {
            BioNLPConstants.None
          }
          val relation = Relation(relationId = bindingRelations(i).relationId + ":" + BioNLPConstants.SameBinding,
            relationType = Label(trueValue = Option(labelValue)), centerMention = centerMention,
            argumentMention = argMention, argumentRole = Label(trueValue = Option(BioNLPConstants.SameBinding)),
            groupId = Set(BioNLPConstants.RelationGroups.BINDING_PAIR), childId = None)
          sent.createRelation(relation)
          doc.createRelation(relation)
        }
      }
      doc
    }

    def build(dataSourceDir: String, maxDocs: Int) = ???

    def generateNoneRelation(mentions: Seq[(Mention, Option[String])], sentence: Sentence, groupId: String) = {
      for (
        mention <- mentions; token <- sentence.tokens if (!sentence.relationExists(mention._1.head, token) && !((mention._1.head.start == token.start) && (mention._1.head.end == token.end)))
      ) yield {
        val centerMention = Mention(mentionId = None, head = token, begin = token,
          end = token)
        val relation = Relation(relationId = mention._1.mentionId.getOrElse("") + ":" + BioNLPConstants.None,
          relationType = Label(trueValue = Option(BioNLPConstants.None)), centerMention = centerMention,
          argumentMention = mention._1, argumentRole = Label(), groupId = Set(groupId), childId = mention._2)
        relation
      }
    }
  }

  /**
   * Construct false relations based on only mentions involved in relation
   */
  class BioNLPGeniaRelationBuilderWithMentionInRelation extends RelationBuilder {

    def build(doc: Document) = {
      doc.sentences.foreach { sent =>
        val relationsSimple = sent.relations.filter(_.groupId(BioNLPConstants.RelationGroups.SIMPLE_EVENT))
        val argMentionChildIds = relationsSimple.map(r => (r.argumentMention, None))
        generateNoneRelation(argMentionChildIds, sent, BioNLPConstants.RelationGroups.SIMPLE_EVENT)
          .foreach { relation =>
            sent.createRelation(relation)
            doc.createRelation(relation)
          }
        val triggerMentionsChildIds = relationsSimple.map(r => (r.centerMention, Some(r.relationId)))
        generateNoneRelation(triggerMentionsChildIds, sent, BioNLPConstants.RelationGroups.COMPLEX_REG)
          .foreach { relation =>
            sent.createRelation(relation)
            doc.createRelation(relation)
          }
        val bindingRelations = sent.relations.filter(relation => relation.centerMention.mentionLabel.trueValue.getOrElse("") == BioNLPConstants.Binding)
          .toIndexedSeq
        for (i <- 0 until bindingRelations.length; j <- i + 1 until bindingRelations.length) {
          val centerMention = bindingRelations(i).argumentMention
          val argMention = bindingRelations(j).argumentMention
          val labelValue = if (bindingRelations(i).relationId == bindingRelations(j).relationId) {
            BioNLPConstants.SameBinding
          } else {
            BioNLPConstants.None
          }
          val relation = Relation(relationId = bindingRelations(i).relationId + ":" + BioNLPConstants.SameBinding,
            relationType = Label(trueValue = Option(labelValue)), centerMention = centerMention,
            argumentMention = argMention, argumentRole = Label(trueValue = Option(BioNLPConstants.SameBinding)),
            groupId = Set(BioNLPConstants.RelationGroups.BINDING_PAIR), childId = None)
          sent.createRelation(relation)
          doc.createRelation(relation)
        }
      }
      doc
    }

    def build(dataSourceDir: String, maxDocs: Int) = ???

    def generateNoneRelation(mentions: Seq[(Mention, Option[String])], sentence: Sentence, groupId: String) = {
      for (
        mention <- mentions; token <- sentence.tokens if (!sentence.relationExists(mention._1.head, token) && !((mention._1.head.start == token.start) && (mention._1.head.end == token.end)))
      ) yield {
        val centerMention = Mention(mentionId = None, head = token, begin = token,
          end = token)
        val relation = Relation(relationId = mention._1.mentionId.getOrElse("") + ":" + BioNLPConstants.None,
          relationType = Label(trueValue = Option(BioNLPConstants.None)), centerMention = centerMention,
          argumentMention = mention._1, argumentRole = Label(),
          groupId = Set(groupId), childId = mention._2)
        relation
      }
    }
  }

  class BioNLPGeniaRelationBuilderWithTokenFilter(dic: Dictionary) extends RelationBuilder {

    def build(doc: Document) = {
      doc.sentences.foreach { sent =>
        val triggerCandidates = sent.tokens.filter(token => dic.exists(token.word))
        val protMentions = sent.getMentionByTrueLabel(BioNLPConstants.Protein)
        /**
         * prot-tri, theme
         */
        val relationsSimple = sent.relations.filter(_.groupId(BioNLPConstants.RelationGroups.SIMPLE_EVENT))
        val relationsSimpleTheme = relationsSimple.filter(_.argumentRole.trueValue.get == BioNLPConstants.Theme)
        relationsSimpleTheme.foreach(r => r.groupId = r.groupId + BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI)
        val argMentionChildIdsTheme = relationsSimpleTheme.map(r => (r.argumentMention, None, r.eventId))
        generateNoneRelation(argMentionChildIdsTheme, triggerCandidates, BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI, BioNLPConstants.ThemeNone, sameRelationFilterRule(relationsSimpleTheme, sent))
          .foreach { relation =>
            sent.createRelation(relation)
            doc.createRelation(relation)
          }
        /**
         * prot-tri-prot, theme-cause
         */
        val simpleThemeEIds = relationsSimpleTheme.map(_.eventId.get).toSet
        val relationsSimpleCause = relationsSimple.filter(_.argumentRole.trueValue.get == BioNLPConstants.Cause)
        val relationsSimpleCause2ProtCause = relationsSimpleCause.filter(r => simpleThemeEIds(r.eventId.get))
        relationsSimpleCause2ProtCause.foreach(r => r.groupId = r.groupId + BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI_PROT)
        val triMentionChildIdsCause = relationsSimpleCause2ProtCause.map(r => (r.centerMention, None, r.eventId))
        generateNoneRelationProt(triMentionChildIdsCause, protMentions, BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI_PROT, BioNLPConstants.CauseNone, sameRelationFilterRule(relationsSimpleCause, sent))
          .foreach { relation =>
            sent.createRelation(relation)
            doc.createRelation(relation)
          }
        /**
         * event-tri, theme
         */
        val relationsComplex = sent.relations.filter(_.groupId(BioNLPConstants.RelationGroups.COMPLEX_REG))
        val relationsComplexTheme = relationsComplex.filter(_.argumentRole.trueValue.get == BioNLPConstants.Theme)
        val relationsComplexThemeEIds = relationsComplexTheme.map(_.eventId.get).toSet
        relationsComplexTheme.foreach(r => r.groupId = r.groupId + BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI)
        val triggerMentionsChildIdsTheme = relationsComplexTheme.map(r => (r.argumentMention, r.childId, r.eventId))
        generateNoneRelation(triggerMentionsChildIdsTheme, triggerCandidates, BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI, BioNLPConstants.ThemeNone, sameRelationFilterRule(relationsComplexTheme, sent))
          .foreach { relation =>
            sent.createRelation(relation)
            doc.createRelation(relation)
          }
        /**
         * event-tri-prot, theme-cause
         */
        val simpleCauseEIds = relationsSimpleCause.map(_.eventId.get).toSet
        val eventTriProtEIds = simpleCauseEIds & relationsComplexThemeEIds
        val eventTriProtPos = relationsSimpleCause.filter(r => eventTriProtEIds(r.eventId.get))
        eventTriProtPos.foreach(r => r.groupId = r.groupId + BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_PROT)
        val triggerMentionsChildIdsThemeTr = eventTriProtPos.map(r => (r.centerMention, None, r.eventId))
        generateNoneRelationProt(triggerMentionsChildIdsThemeTr, protMentions, BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_PROT, BioNLPConstants.CauseNone, sameRelationFilterRule(eventTriProtPos, sent))
          .foreach { relation =>
            sent.createRelation(relation)
            doc.createRelation(relation)
          }
        /**
         * prot-trig-event, theme-cause
         */
        val relationsComplexCause = relationsComplex.filter(_.argumentRole.trueValue.get == BioNLPConstants.Cause)
        val relationsComplexCause2ProtTheme = relationsComplexCause.filter(r => simpleThemeEIds(r.eventId.get))
        relationsComplexCause2ProtTheme.foreach(r => r.groupId = r.groupId + BioNLPConstants.RelationGroups.COMPLEX_REG_PROT_TRI_EVENT)
        val triggerMentionsChildIdsCauseTr = relationsComplexCause2ProtTheme.map(r => (r.centerMention, r.childId, r.eventId))
        generateNoneRelationProtTri(triggerMentionsChildIdsCauseTr, triggerCandidates, BioNLPConstants.RelationGroups.COMPLEX_REG_PROT_TRI_EVENT, BioNLPConstants.CauseNone, sameRelationFilterRule(relationsComplexCause2ProtTheme, sent))
          .foreach { relation =>
            sent.createRelation(relation)
            doc.createRelation(relation)
          }
        /**
         * event-trig-event, theme-cause
         */
        val relationsComplexTheme2TrigCause = relationsComplexCause.filter(r => relationsComplexThemeEIds(r.eventId.get))
        relationsComplexTheme2TrigCause.foreach(r => r.groupId = r.groupId + BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_EVENT)
        val triggerMentionsChildIdsCauseEvent = relationsComplexTheme2TrigCause.map(r => (r.centerMention, r.childId, r.eventId))
        generateNoneRelationProtTri(triggerMentionsChildIdsCauseEvent, triggerCandidates, BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_EVENT, BioNLPConstants.CauseNone, sameRelationFilterRule(relationsComplexTheme2TrigCause, sent))
          .foreach { relation =>
            sent.createRelation(relation)
            doc.createRelation(relation)
          }
        /**
         * Binding pair
         */
        val bindingRelations = sent.relations.filter(relation => relation.centerMention.mentionLabel.trueValue.getOrElse("") == BioNLPConstants.Binding)
          .toIndexedSeq
        for (i <- 0 until bindingRelations.length; j <- i + 1 until bindingRelations.length) {
          val arg1 = bindingRelations(i).argumentMention
          val arg2 = bindingRelations(j).argumentMention
          val labelValue = if (bindingRelations(i).relationId == bindingRelations(j).relationId) {
            BioNLPConstants.SameBinding
          } else {
            BioNLPConstants.None
          }
          val relation = Relation(relationId = bindingRelations(i).relationId + ":" + BioNLPConstants.SameBinding,
            relationType = Label(trueValue = Option(labelValue)), centerMention = arg1,
            argumentMention = arg2, argumentRole = Label(trueValue = Option(BioNLPConstants.SameBinding)),
            groupId = Set(BioNLPConstants.RelationGroups.BINDING_PAIR), childId = None)
          sent.createRelation(relation)
          doc.createRelation(relation)
        }
      }
      doc
    }

    def build(dataSourceDir: String, maxDocs: Int) = ???

  }

  /**
   * This is for more compact classification.
   * 1. Generate prot-tri (Theme) excluding regulation events
   * 2. Generate prot-tri, tri-tri (Theme) for only regulation events
   * 3. Generate same-binding
   * 4. Generate tri-prot, tri-tri (Cause) for only regulation events
   */
  class BioNLPGeniaRelationBuilderWithTokenFilterCompact(dic: Dictionary) extends RelationBuilder {

    def build(doc: Document) = {
      doc.sentences.foreach { sent =>
        val triggerCandidates = sent.tokens.filter(token => dic.exists(token.word))
        val protMentions = sent.getMentionByTrueLabel(BioNLPConstants.Protein)
        /**
         * prot-tri (Theme) excluding regulation events
         */
        val relationsSimple = sent.relations.filter(r => BioNLPConstants.isSimpleWithBinding(r.relationType.trueValue.get))
        val relationsSimpleTheme = relationsSimple.filter(_.argumentRole.trueValue.get == BioNLPConstants.Theme)
        relationsSimpleTheme.foreach(r => r.groupId = Set(BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI))
        val argMentionChildIdsTheme = relationsSimpleTheme.map(r => (r.argumentMention, None, r.eventId))
        generateNoneRelation(argMentionChildIdsTheme, triggerCandidates, BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI,
          BioNLPConstants.ThemeNone, sameRelationFilterRule(relationsSimpleTheme, sent))
          .foreach { relation =>
            sent.createRelation(relation)
            doc.createRelation(relation)
          }
        /**
         * prot-tri, tri-tri (Theme) for only regulation events
         */
        val relationsReg = sent.relations.filter(r => BioNLPConstants.isComplex(r.relationType.trueValue.get))
        val relationsRegTheme = relationsReg.filter(_.argumentRole.trueValue.get == BioNLPConstants.Theme)
        val relationsRegThemeSimple = relationsRegTheme.filter(_.groupId(BioNLPConstants.RelationGroups.SIMPLE_EVENT))
        relationsRegThemeSimple.foreach(r => r.groupId = Set(BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI))
        val relationsRegSimpleArgTh = relationsRegThemeSimple.map(r => (r.argumentMention, None, r.eventId))
        generateNoneRelation(relationsRegSimpleArgTh, triggerCandidates, BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI,
          BioNLPConstants.ThemeNone, sameRelationFilterRule(relationsRegThemeSimple, sent))
          .foreach { relation =>
            sent.createRelation(relation)
            doc.createRelation(relation)
          }
        val relationsRegThemeComplex = relationsRegTheme.filter(_.groupId(BioNLPConstants.RelationGroups.COMPLEX_REG))
        relationsRegThemeComplex.foreach(r => r.groupId = Set(BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI))
        val relationsRegComplexCenTh = relationsRegThemeComplex.map(r => (r.centerMention, None, r.eventId))
        generateNoneRelation(relationsRegComplexCenTh, triggerCandidates, BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI,
          BioNLPConstants.ThemeNone, sameRelationFilterRule(relationsRegThemeComplex, sent))
          .foreach { relation =>
            sent.createRelation(relation)
            doc.createRelation(relation)
          }
        /**
         * tri-prot, tri-tri (Cause) for only regulation events
         */
        val relationsRegCause = relationsReg.filter(_.argumentRole.trueValue.get == BioNLPConstants.Cause)
        val relationsRegCauseSimple = relationsRegCause.filter(_.groupId(BioNLPConstants.RelationGroups.SIMPLE_EVENT))
        relationsRegCauseSimple.foreach(r => r.groupId = Set(BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_PROT))
        val relationsRegSimpleCa = relationsRegCauseSimple.map(r => (r.centerMention, None, r.eventId))
        generateNoneRelationProt(relationsRegSimpleCa, protMentions, BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_PROT,
          BioNLPConstants.CauseNone, sameRelationFilterRule(relationsRegCauseSimple, sent))
          .foreach { relation =>
            sent.createRelation(relation)
            doc.createRelation(relation)
          }
        val relationsRegCauseComplex = relationsRegCause.filter(_.groupId(BioNLPConstants.RelationGroups.COMPLEX_REG))
        relationsRegCauseComplex.foreach(r => r.groupId = Set(BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_PROT))
        val relationsRegComplexCa = relationsRegCauseComplex.map(r => (r.centerMention, None, r.eventId))
        generateNoneRelationProtTri(relationsRegComplexCa, triggerCandidates, BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_PROT,
          BioNLPConstants.CauseNone, sameRelationFilterRule(relationsRegCauseComplex, sent))
          .foreach { relation =>
            sent.createRelation(relation)
            doc.createRelation(relation)
          }
        /**
         * Binding pair
         */
        val bindingRelations = sent.relations.filter(relation => relation.centerMention.mentionLabel.trueValue.getOrElse("") == BioNLPConstants.Binding)
          .toIndexedSeq
        for (i <- 0 until bindingRelations.length; j <- i + 1 until bindingRelations.length) {
          val arg1 = bindingRelations(i).argumentMention
          val arg2 = bindingRelations(j).argumentMention
          val labelValue = if (bindingRelations(i).relationId == bindingRelations(j).relationId) {
            BioNLPConstants.SameBinding
          } else {
            BioNLPConstants.None
          }
          val relation = Relation(relationId = bindingRelations(i).relationId + ":" + BioNLPConstants.SameBinding,
            relationType = Label(trueValue = Option(labelValue)), centerMention = arg1,
            argumentMention = arg2, argumentRole = Label(trueValue = Option(BioNLPConstants.SameBinding)),
            groupId = Set(BioNLPConstants.RelationGroups.BINDING_PAIR), childId = None)
          sent.createRelation(relation)
          doc.createRelation(relation)
        }
      }
      doc
    }

    def build(dataSourceDir: String, maxDocs: Int) = ???

  }

}