/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.dataset.generation

import bioee.domains._
import bioee.utils.BioNLPConstants
import bioee.utils.Dictionary

trait BioNLPEventBuilderComponent extends EventBuilderComponent {

  class BioNLPEventBuilder(dic: Dictionary) extends EventBuilder {

    def build(doc: Document) = {
      val sents = doc.sentences
      sents.foreach { sent =>
        val relsThem = sent.relations.filter(_.argumentRole.trueValue.get == BioNLPConstants.Theme)
          .map(r => r.eventId.get -> r).toMap
        val relsCausOrThem2 = sent.relations.filter(r =>
          (r.argumentRole.trueValue.get == BioNLPConstants.Cause || r.argumentRole.trueValue.get == BioNLPConstants.Theme2))
          .map(r => r.eventId.get -> r).toMap
        var eId2E = Map[String, Event]()
        relsThem.foreach { pair =>
          val (eId, r) = pair
          if (!eId2E.keySet(eId)) {
            val ret = build(sent, doc, eId2E, relsCausOrThem2, relsThem, r)
            eId2E = ret._2
          }
        }
        var c = -1
        def cId() = {
          c = c + 1
          s":$c"
        }
        val triCans = sent.tokens.filter(token => dic.exists(token.word))
        val protMens = sent.getMentionByTrueLabel(BioNLPConstants.Protein)
        sent.events = sent.events.map(e => e.id -> e).toMap.map(_._2).toSeq
        val sEvtsThem = sent.events.filter(_.childTheme.isEmpty)
        sEvtsThem.foreach { e =>
          e.groupId = e.groupId + BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI
        }
        val bEvts = sent.events.filter(e => e.eventType.trueValue.get == BioNLPConstants.Binding && e.arguments.length > 1)
        bEvts.foreach { e =>
          e.groupId = e.groupId + BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI_PROT
        }
        val cEvtsThem = sent.events.filter(_.childTheme.isDefined)
        cEvtsThem.foreach { e =>
          e.groupId = e.groupId + BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI
        }
        val cEvtsCaus = sent.events.filter(e => BioNLPConstants.isComplex(e.eventType.trueValue.get))
        cEvtsCaus.filter(e => e.arguments.length > 1 || e.childCause.isDefined ||
          (e.childTheme.isDefined && e.arguments.length > 0)).foreach { e =>
          e.groupId = e.groupId + BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_PROT
        }
        for (
          e <- sEvtsThem; tri <- triCans if (!sEvtsThem.exists(e1 => e1.arguments(0).argument.head.indexInSentence == e.arguments(0).argument.head.indexInSentence
            && e1.trigger.head.indexInSentence == tri.indexInSentence))
        ) {
          val trigger = Mention(mentionId = None, head = tri, begin = tri,
            end = tri)
          val negEvt = e.copy(id = e.id + cId, trigger = trigger, eventType = Label(trueValue = Some(BioNLPConstants.None)))
          sent.createEvent(negEvt)
        }
        for (e <- bEvts; prot <- protMens if (!bEvts.exists(e1 => protArgExists(e1, e, prot)))) {
          val args = Seq(e.arguments(0).copy(), RoleMention(role = Label(trueValue = Some(BioNLPConstants.Theme2)), prot))
          val negEvt = e.copy(id = e.id + cId, eventType = Label(trueValue = Some(BioNLPConstants.None)), arguments = args,
            groupId = e.groupId + BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI_PROT)
          sent.createEvent(negEvt)
        }
        for (
          e <- cEvtsThem; tri <- triCans if (!cEvtsThem.exists(e1 => e1.childTheme.get.id == e.childTheme.get.id &&
            e.trigger.head.indexInSentence == tri.indexInSentence))
        ) {
          val trigger = Mention(mentionId = None, head = tri, begin = tri,
            end = tri)
          val negEvt = e.copy(id = e.id + cId, trigger = trigger, eventType = Label(trueValue = Some(BioNLPConstants.None)))
          sent.createEvent(negEvt)
        }
        for (e <- cEvtsCaus; prot <- protMens if (!cEvtsCaus.exists(e1 => protArgExists(e1, e, prot)))) {
          val argCaus = RoleMention(role = Label(trueValue = Some(BioNLPConstants.Cause)), prot)
          val args = if (e.childTheme.isEmpty) {
            Seq(e.arguments(0).copy(), argCaus)
          } else {
            Seq(argCaus)
          }
          val negEvt = e.copy(id = e.id + cId, eventType = Label(trueValue = Some(BioNLPConstants.None)), arguments = args, childCause = None,
            groupId = e.groupId + BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_PROT)
          sent.createEvent(negEvt)
        }
        for (e <- cEvtsCaus; ec <- cEvtsCaus if (!cEvtsCaus.exists(e1 => evtArgExists(e1, e, ec)))) {
          val args = if (e.childTheme.isEmpty) {
            Seq(e.arguments(0).copy())
          } else {
            Seq()
          }
          val negEvt = e.copy(id = e.id + cId, eventType = Label(trueValue = Some(BioNLPConstants.None)), arguments = args, childCause = Some(ec),
            groupId = e.groupId + BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_PROT)
          sent.createEvent(negEvt)
        }
      }
      doc
    }

    def build(sent: Sentence, doc: Document, eId2E: Map[String, Event],
      relsCausOrThem2: Map[String, Relation], relsThem: Map[String, Relation], rHead: Relation): (Option[Event], Map[String, Event]) = {
      val eId = rHead.eventId.get
      val trigger = rHead.centerMention.copy()
      val (rms1, rs1, childTheme, eId2Es, isOutThem) = if (rHead.childId.isEmpty) {
        val rm1 = RoleMention(rHead.argumentRole, rHead.argumentMention)
        (Seq(rm1), Seq(rHead), None, eId2E, false)
      } else {
        val childId = rHead.childId.get
        if (eId2E.get(childId).isEmpty) {
          if (relsThem.get(childId).isEmpty) {
            (Seq(), Seq(), None, eId2E, true)
          } else {
            val (childTheme, eId2Es) = build(sent, doc, eId2E, relsCausOrThem2, relsThem, relsThem(childId))
            if (childTheme.isEmpty)
              (Seq(), Seq(), None, eId2E, true)
            else
              (Seq(), Seq(rHead), childTheme, eId2Es, false)
          }
        } else {
          val childTheme = eId2E(childId)
          (Seq(), Seq(rHead), Option(childTheme), eId2E, false)
        }
      }
      val mayArg2 = relsCausOrThem2.get(eId)
      val (rms2, rs2, childCause, eId2Es2, isOutCaus) = if (mayArg2.isEmpty) {
        (Seq(), Seq(), None, eId2Es, false)
      } else {
        val r2 = mayArg2.get
        if (r2.childId.isEmpty) {
          val cc: Option[Event] = None
          val rm2 = RoleMention(r2.argumentRole, r2.argumentMention)
          (Seq(rm2), Seq(r2), None, eId2Es, false)
        } else {
          val childId2 = r2.childId.get
          if (eId2Es.get(childId2).isEmpty) {
            if (relsThem.get(childId2).isEmpty) {
              (Seq(), Seq(), None, eId2Es, true)
            } else {
              val (childCause, eId2Ess) = build(sent, doc, eId2Es, relsCausOrThem2, relsThem, relsThem(childId2))
              if (childCause.isEmpty)
                (Seq(), Seq(), None, eId2Es, true)
              else
                (Seq(), Seq(r2), childCause, eId2Ess, false)
            }
          } else {
            val childCause = eId2Es(childId2)
            (Seq(), Seq(r2), Option(childCause), eId2Es, false)
          }
        }
      }
      if (isOutThem || isOutCaus) (None, eId2Es2)
      else {
        val mayEvent = Some(Event(id = eId, eventType = rHead.relationType, trigger = trigger,
          arguments = rms1 ++ rms2, relations = rs1 ++ rs2, childTheme = childTheme, childCause = childCause))
        val events = (childTheme ++ childCause ++ mayEvent).toSeq
        val eId2Eret = eId2Es2 ++ events.map { e =>
          sent.createEvent(e)
          e.id -> e
        }
        (mayEvent, eId2Eret)
      }
    }
  }

  def protArgExists(e1: Event, e: Event, protMen: Mention) = {
    if (e1.arguments.length < 2) false
    else {
      e1.trigger.head.indexInSentence == e.trigger.head.indexInSentence &&
        e1.arguments(1).argument.head.indexInSentence == protMen.head.indexInSentence
    }
  }

  def evtArgExists(e1: Event, e: Event, ec: Event) = {
    if (e1.arguments.length > 1 || e1.childCause.isEmpty) false
    else {
      e1.trigger.head.indexInSentence == e.trigger.head.indexInSentence &&
        e1.childCause.get.id == ec.id
    }
  }

}