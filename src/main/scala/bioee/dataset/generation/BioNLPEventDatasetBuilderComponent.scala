/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.dataset.generation

import bioee.text.preprocessing.PreprocessingComponent
import bioee.domains._
import bioee.utils.BioNLPConstants
import breeze.linalg._
import bioee.utils.Word2Vec
import java.io.File
import bioee.utils.BioNLPUtils
import com.typesafe.scalalogging.slf4j.LazyLogging
import bioee.utils.TextHelpers
import bioee.utils.RichFile
import bioee.utils.RichFile.enrichFile
import java.io.PrintWriter
import bioee.utils.Dictionary
import bioee.utils.BioNLPUtils._
import bioee.dataset.generation.BioNLPExampleBuilderComponent.BioNLPGeniaExampleBuilder

trait BioNLPEventDatasetBuilderComponent extends DatasetBuilderComponent {
  this: PreprocessingComponent with EventBuilderComponent with ExampleBuilderComponent =>

  class BioNLPEventDatasetBuilder extends DatasetBuilder with LazyLogging {

    def build(doc: Document) = {
      tokenizer.annotate(doc)
      sentenceSplitter.annotate(doc)
      mentionTaggger.annotate(doc)
      relationLoader.annotate(doc)
      eventBuilder.build(doc)
      exampleBuilder.build(doc)
    }

    def buildFromDir(dataSourceDir: String, sentDir: String, maxDocs: Int = Int.MaxValue,
      simpleProtTriDs: Dataset, simpleProtTriProtDs: Dataset,
      complexEventTriThemeDs: Dataset, complexEventTriProtCauseDs: Dataset) = {
      val dataDir = new File(dataSourceDir)
      val docs = loadDocs(dir = dataDir, maxCount = maxDocs)

      docs.foreach { doc =>
        build(doc)
        val sents = doc.sentences
        var i1 = 0
        var i2 = 0
        var i3 = 0
        var i4 = 0
        sents.foreach { sent =>
          val sentId = doc.docId + "." + sent.indexInDocument
          val sentPath = sentDir + sentId
          sent.saveExample(path = sentPath)
          val events = sent.events
          val simpleProtTriRs = events.filter(_.groupId(BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI))
          val simpleProtTriProtRs = events.filter(_.groupId(BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI_PROT))
          val complexEventTriThemeRs = events.filter(_.groupId(BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI))
          val complexEventTriProtCauseRs = events.filter(_.groupId(BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_PROT))

          simpleProtTriRs.foreach { e =>
            val example = e.exampleTheme.get
            val labelIndex = simpleProtTriDs.label2Index(example.label.trueValue.get)
            example.labelIndex = Option(labelIndex)
            val pathToSave = simpleProtTriDs.datasetDirPath + doc.textSourceFile.get + "." + i1 + "." + example.label.trueValue.get + "." + labelIndex
            example.saveEvent(path = pathToSave, docId = doc.docId, e = e, sentPath = sentId)
            i1 = i1 + 1
          }
          simpleProtTriProtRs.foreach { e =>
            val example = e.exampleCause.get
            val labelIndex = simpleProtTriProtDs.label2Index(example.label.trueValue.get)
            example.labelIndex = Option(labelIndex)
            val pathToSave = simpleProtTriProtDs.datasetDirPath + doc.textSourceFile.get + "." + i2 + "." + example.label.trueValue.get + "." + labelIndex
            example.saveEvent(path = pathToSave, docId = doc.docId, e = e, sentPath = sentId)
            i2 = i2 + 1
          }
          complexEventTriThemeRs.foreach { e =>
            val example = e.exampleTheme.get
            val labelIndex = complexEventTriThemeDs.label2Index(example.label.trueValue.get)
            example.labelIndex = Option(labelIndex)
            val pathToSave = complexEventTriThemeDs.datasetDirPath + doc.textSourceFile.get + "." + i3 + "." + example.label.trueValue.get + "." + labelIndex
            example.saveEvent(path = pathToSave, docId = doc.docId, e = e, sentPath = sentId)
            i3 = i3 + 1
          }
          complexEventTriProtCauseRs.foreach { e =>
            val example = e.exampleCause.get
            val labelIndex = complexEventTriProtCauseDs.label2Index(example.label.trueValue.get)
            example.labelIndex = Option(labelIndex)
            val pathToSave = complexEventTriProtCauseDs.datasetDirPath + doc.textSourceFile.get + "." + i4 + "." + example.label.trueValue.get + "." + labelIndex
            example.saveEvent(path = pathToSave, docId = doc.docId, e = e, sentPath = sentId)
            i4 = i4 + 1
          }
        }
      }
      Map(simpleProtTriDs.datasetId -> simpleProtTriDs,
        simpleProtTriProtDs.datasetId -> simpleProtTriProtDs,
        complexEventTriThemeDs.datasetId -> complexEventTriThemeDs,
        complexEventTriProtCauseDs.datasetId -> complexEventTriProtCauseDs)
    }
  }
}