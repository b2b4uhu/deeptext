/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.dataset.generation

import bioee.text.preprocessing.PreprocessingComponent
import bioee.domains._
import bioee.utils.BioNLPConstants
import breeze.linalg._
import bioee.utils.Word2Vec
import java.io.File
import bioee.utils.BioNLPUtils
import com.typesafe.scalalogging.slf4j.LazyLogging
import bioee.utils.TextHelpers
import bioee.utils.RichFile
import bioee.utils.RichFile.enrichFile
import java.io.PrintWriter
import bioee.utils.Dictionary
import bioee.utils.BioNLPUtils._
import bioee.dataset.generation.BioNLPExampleBuilderComponent.BioNLPGeniaExampleBuilder

trait BioNLPDatasetBuilderComponent extends DatasetBuilderComponent {
  this: PreprocessingComponent with RelationBuilderComponent with ExampleBuilderComponent =>

  class BioNLPGeniaDatasetBuilder extends DatasetBuilder with LazyLogging {

    def build(doc: Document) = {
      tokenizer.annotate(doc)
      sentenceSplitter.annotate(doc)
      mentionTaggger.annotate(doc)
      relationLoader.annotate(doc)
      relationBuilder.build(doc)
      exampleBuilder.build(doc)
    }

    def buildFromDir(dataSourceDir: String, maxDocs: Int = Int.MaxValue,
      simpleProtTriThemeDataset: Dataset, simpleProtTriProtCauseDataset: Dataset,
      complexEventTriThemeDataset: Dataset, complexEventTriProtCauseDataset: Dataset,
      complexProtTriEventThemeDataset: Dataset, complexEventTriEventCauseDataset: Dataset,
      bindingPairDataset: Dataset) = {
      val dataDir = new File(dataSourceDir)
      val docs = loadDocs(dir = dataDir, maxCount = maxDocs)

      docs.foreach { doc =>
        build(doc)
        val sents = doc.sentences
        var i1 = 0
        var i2 = 0
        var i3 = 0
        var i4 = 0
        var i5 = 0
        var i6 = 0
        var i7 = 0
        sents.foreach { sent =>
          val relations = sent.relations
          val simpleProtTriThemeRelations = relations.filter(_.groupId(BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI))
          val simpleProtTriProtCauseRelations = relations.filter(_.groupId(BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI_PROT))
          val complexEventTriThemeRelations = relations.filter(_.groupId(BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI))
          val complexEventTriProtCauseRelations = relations.filter(_.groupId(BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_PROT))
          val complexProtTriEventThemeRelations = relations.filter(_.groupId(BioNLPConstants.RelationGroups.COMPLEX_REG_PROT_TRI_EVENT))
          val complexEventTriEventCauseRelations = relations.filter(_.groupId(BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_EVENT))
          val bindingPairRelations = relations.filter(_.groupId(BioNLPConstants.RelationGroups.BINDING_PAIR))
          simpleProtTriThemeRelations.zipWithIndex.foreach { exampleAndindex =>
            val (relation, index) = exampleAndindex
            val example = relation.example.get
            val labelIndex = simpleProtTriThemeDataset.label2Index(example.label.trueValue.get)
            example.labelIndex = Option(labelIndex)
            val pathToSave = simpleProtTriThemeDataset.datasetDirPath + doc.textSourceFile.get + "." + i1 + "." + example.label.trueValue.get + "." + labelIndex
            example.save(path = pathToSave, docId = doc.docId, r = relation)
            i1 = i1 + 1
          }
          simpleProtTriProtCauseRelations.zipWithIndex.foreach { exampleAndindex =>
            val (relation, index) = exampleAndindex
            val example = relation.example.get
            val labelIndex = simpleProtTriProtCauseDataset.label2Index(example.label.trueValue.get)
            example.labelIndex = Option(labelIndex)
            val pathToSave = simpleProtTriProtCauseDataset.datasetDirPath + doc.textSourceFile.get + "." + i2 + "." + example.label.trueValue.get + "." + labelIndex
            example.save(path = pathToSave, docId = doc.docId, r = relation)
            i2 = i2 + 1
          }
          complexEventTriThemeRelations.zipWithIndex.foreach { exampleAndindex =>
            val (relation, index) = exampleAndindex
            val example = relation.example.get
            val labelIndex = complexEventTriThemeDataset.label2Index(example.label.trueValue.get)
            example.labelIndex = Option(labelIndex)
            val pathToSave = complexEventTriThemeDataset.datasetDirPath + doc.textSourceFile.get + "." + i3 + "." + example.label.trueValue.get + "." + labelIndex
            example.save(path = pathToSave, docId = doc.docId, r = relation)
            i3 = i3 + 1
          }
          complexEventTriProtCauseRelations.zipWithIndex.foreach { exampleAndindex =>
            val (relation, index) = exampleAndindex
            val example = relation.example.get
            val labelIndex = complexEventTriProtCauseDataset.label2Index(example.label.trueValue.get)
            example.labelIndex = Option(labelIndex)
            val pathToSave = complexEventTriProtCauseDataset.datasetDirPath + doc.textSourceFile.get + "." + i4 + "." + example.label.trueValue.get + "." + labelIndex
            example.save(path = pathToSave, docId = doc.docId, r = relation)
            i4 = i4 + 1
          }
          complexProtTriEventThemeRelations.zipWithIndex.foreach { exampleAndindex =>
            val (relation, index) = exampleAndindex
            val example = relation.example.get
            val labelIndex = complexProtTriEventThemeDataset.label2Index(example.label.trueValue.get)
            example.labelIndex = Option(labelIndex)
            val pathToSave = complexProtTriEventThemeDataset.datasetDirPath + doc.textSourceFile.get + "." + i5 + "." + example.label.trueValue.get + "." + labelIndex
            example.save(path = pathToSave, docId = doc.docId, r = relation)
            i5 = i5 + 1
          }
          complexEventTriEventCauseRelations.zipWithIndex.foreach { exampleAndindex =>
            val (relation, index) = exampleAndindex
            val example = relation.example.get
            val labelIndex = complexEventTriEventCauseDataset.label2Index(example.label.trueValue.get)
            example.labelIndex = Option(labelIndex)
            val pathToSave = complexEventTriEventCauseDataset.datasetDirPath + doc.textSourceFile.get + "." + i6 + "." + example.label.trueValue.get + "." + labelIndex
            example.save(path = pathToSave, docId = doc.docId, r = relation)
            i6 = i6 + 1
          }
          bindingPairRelations.zipWithIndex.foreach { exampleAndindex =>
            val (relation, index) = exampleAndindex
            val example = relation.example.get
            val labelIndex = bindingPairDataset.label2Index(example.label.trueValue.get)
            example.labelIndex = Option(labelIndex)
            val pathToSave = bindingPairDataset.datasetDirPath + doc.textSourceFile.get + "." + i7 + "." + example.label.trueValue.get + "." + labelIndex
            example.save(path = pathToSave, docId = doc.docId, r = relation)
            i7 = i7 + 1
          }
        }
      }
      Map(simpleProtTriThemeDataset.datasetId -> simpleProtTriThemeDataset,
        simpleProtTriProtCauseDataset.datasetId -> simpleProtTriProtCauseDataset,
        complexEventTriThemeDataset.datasetId -> complexEventTriThemeDataset,
        complexEventTriProtCauseDataset.datasetId -> complexEventTriProtCauseDataset,
        complexProtTriEventThemeDataset.datasetId -> complexProtTriEventThemeDataset,
        complexEventTriEventCauseDataset.datasetId -> complexEventTriEventCauseDataset,
        bindingPairDataset.datasetId -> bindingPairDataset)
    }

    def devReadWrite(dataSourceDir: String, writeDirPath: String) = {
      val dataDir = new File(dataSourceDir)
      val docs = loadDocs(dir = dataDir)
      docs.foreach { doc =>
        tokenizer.annotate(doc)
        sentenceSplitter.annotate(doc)
        mentionTaggger.annotate(doc)
        relationLoader.annotate(doc)
        doc.relations.map { relations =>
          relations
        }
      }
    }

    def reportStats(dataSourceDir: String) = {
      val dataDir = new File(dataSourceDir)
      val docs = loadDocs(dir = dataDir)
      var nsentences = 0
      var maxSenLen = -1
      var minSenLen = 100000
      var avgSenLen = 0
      var senHistLen = Map[Int, Int]()
      var maxSen: Sentence = null
      var minSen: Sentence = null
      docs.foreach { doc =>
        tokenizer.annotate(doc)
        sentenceSplitter.annotate(doc)
        nsentences += doc.sentences.size
        val sents = doc.sentences
        sents.foreach { sent =>
          val sen = sent.tokens
          senHistLen += (sen.size -> (senHistLen.getOrElse(sen.size, 0) + 1))
          if (minSenLen > sen.size) {
            minSenLen = sen.size
            minSen = sent
          }
          if (maxSenLen < sen.size) {
            maxSenLen = sen.size
            maxSen = sent
          }
        }
      }
      val senHistLenSorted = senHistLen.toSeq.sortBy(_._1)
      avgSenLen = senHistLenSorted.foldLeft(0)((total, pair) => total + (pair._1 * pair._2)) / senHistLenSorted.foldLeft(0)((total, pair) => total + pair._2)
      println("Min len sentence: " + senHistLenSorted.head._1)
      println("Min sentence: " + minSen.toString)
      println("Max len sentence: " + senHistLenSorted.last._1)
      println("Max sentence: " + maxSen.toString)
      println("Avg sentence: " + avgSenLen)
      println()
      println("Histogram sentence: ")
      println(senHistLenSorted.mkString("\n").replace("(", "").replace(")", ""))

    }
  }

  class BioNLPGeniaDatasetBuilderCompact extends DatasetBuilder with LazyLogging {

    def build(doc: Document) = {
      tokenizer.annotate(doc)
      sentenceSplitter.annotate(doc)
      mentionTaggger.annotate(doc)
      relationLoader.annotate(doc)
      relationBuilder.build(doc)
      exampleBuilder.build(doc)
    }

    def buildFromDir(dataSourceDir: String, maxDocs: Int = Int.MaxValue,
      simpleProtTriDs: Dataset,
      complexEventTriThemeDs: Dataset, complexEventTriProtCauseDs: Dataset,
      bindingPairDs: Dataset) = {
      val dataDir = new File(dataSourceDir)
      val docs = loadDocs(dir = dataDir, maxCount = maxDocs)

      docs.foreach { doc =>
        build(doc)
        val sents = doc.sentences
        var i1 = 0
        var i2 = 0
        var i3 = 0
        var i4 = 0
        sents.foreach { sent =>
          val relations = sent.relations
          val simpleProtTriRs = relations.filter(_.groupId(BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI))
          val complexEventTriThemeRs = relations.filter(_.groupId(BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI))
          val complexEventTriProtCauseRs = relations.filter(_.groupId(BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_PROT))
          val bindingPairRs = relations.filter(_.groupId(BioNLPConstants.RelationGroups.BINDING_PAIR))
          simpleProtTriRs.foreach { relation =>
            val example = relation.example.get
            val labelIndex = simpleProtTriDs.label2Index(example.label.trueValue.get)
            example.labelIndex = Option(labelIndex)
            val pathToSave = simpleProtTriDs.datasetDirPath + doc.textSourceFile.get + "." + i1 + "." + example.label.trueValue.get + "." + labelIndex
            example.save(path = pathToSave, docId = doc.docId, r = relation)
            i1 = i1 + 1
          }
          complexEventTriThemeRs.foreach { relation =>
            val example = relation.example.get
            val labelIndex = complexEventTriThemeDs.label2Index(example.label.trueValue.get)
            example.labelIndex = Option(labelIndex)
            val pathToSave = complexEventTriThemeDs.datasetDirPath + doc.textSourceFile.get + "." + i2 + "." + example.label.trueValue.get + "." + labelIndex
            example.save(path = pathToSave, docId = doc.docId, r = relation)
            i2 = i2 + 1
          }
          complexEventTriProtCauseRs.foreach { relation =>
            val example = relation.example.get
            val labelIndex = complexEventTriProtCauseDs.label2Index(example.label.trueValue.get)
            example.labelIndex = Option(labelIndex)
            val pathToSave = complexEventTriProtCauseDs.datasetDirPath + doc.textSourceFile.get + "." + i3 + "." + example.label.trueValue.get + "." + labelIndex
            example.save(path = pathToSave, docId = doc.docId, r = relation)
            i3 = i3 + 1
          }
          bindingPairRs.foreach { relation =>
            val example = relation.example.get
            val labelIndex = bindingPairDs.label2Index(example.label.trueValue.get)
            example.labelIndex = Option(labelIndex)
            val pathToSave = bindingPairDs.datasetDirPath + doc.textSourceFile.get + "." + i4 + "." + example.label.trueValue.get + "." + labelIndex
            example.save(path = pathToSave, docId = doc.docId, r = relation)
            i4 = i4 + 1
          }
        }
      }
      Map(simpleProtTriDs.datasetId -> simpleProtTriDs,
        complexEventTriThemeDs.datasetId -> complexEventTriThemeDs,
        complexEventTriProtCauseDs.datasetId -> complexEventTriProtCauseDs,
        bindingPairDs.datasetId -> bindingPairDs)
    }
  }
}